/* 
    Practise Problem
        3. Write a program to find an element in a linked list by name.
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node
{
    int number;
    node *p_next;
};

//Global variable
node *p_list = NULL;

//Function headers
void srandSeed();
int randRange(int low, int high);
void display(node *p_head);
node *search(int number);
node *getNewNode(node *p_head);

//The program starts here
int main()
{
    srandSeed();
    for (int i = 0; i < 5; i++)
        p_list = getNewNode(p_list);

    display(p_list);

    cout << "\n The value is at the address: " << search(5);
    cout << "\n Zero means there is no such a value!\n";

    delete p_list;
    p_list = NULL;
}

node *search(int number)
{
    node *p_temp = p_list;

    while (p_temp != NULL)
    {
        if (p_temp->number == number)
            break;
        p_temp = p_temp->p_next;
    }
    return p_temp;
}

node *getNewNode(node *p_head)
{
    node *p_temp = new node;
    p_temp->number = randRange(1, 9);
    p_temp->p_next = p_head;
    return p_temp;
}

void display(node *p_head)
{
    while (p_head != NULL)
    {
        cout << p_head << " | " << p_head->number << endl;
        p_head = p_head->p_next;
    }
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << "srandSeed: " << seed << " \n";
    srand(seed);
}
