/*
    Practise Problem
        4. Implement a sort function that takes a vector of any type and sorts
   the values by their natural sort order (the order you get from using < or >).
*/

#include <iostream>
#include <vector>

using namespace std;

// Function Heders
template <typename T>
void sort(vector<T>& vec);
template <typename T>
void print(const vector<T>& vec);

// The program starts here
int main() {
  vector<int> vec{2, 3, 5, 4, 1};

  sort(vec);
  print(vec);
}

template <typename T>
void sort(vector<T>& vec) {
  for (size_t i = 0; i < vec.size() - 1; ++i) {
    for (size_t j = 0; j < vec.size() - i - 1; ++j) {
      if (vec.at(j) > vec.at(j + 1))
        swap(vec.at(j), vec.at(j + 1));
    }
  }
}

template <typename T>
void print(const vector<T>& vec) {
  for (auto& i : vec) {
    cout << " " << i;
  }
  cout << endl;
}