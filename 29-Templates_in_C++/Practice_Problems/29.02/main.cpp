/*
    Practise Problem
        2. Modify the vector replacement class implemented as a practice problem
   in chapter 24, but make it a template so that it is can store any type.
*/

#include "Vector.cpp"
#include "Vector.hxx"

using namespace std;

// The program starts here
int main() {
  cout << "\nTest: Contructor with negative size: \n";
  Vector<float> arr(-1);
  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if pushBack scale size \n\n";
  arr.pushBack(2.2);
  arr.pushBack(9.9);
  arr.print();

  arr.pushBack(3.3);
  arr.print();
  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if u can set value out of index  \n\n";
  cout << "Positive num:  \n";
  arr.set(5, 15.15);
  cout << "Negative num:  \n";
  arr.set(-1, 15.2);
  cout << "Correct (2,15):  \n";
  arr.set(2, 15.4);
  arr.print();

  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if pushFront works fine  \n\n";
  arr.pushFront(6.6);
  arr.print();

  arr.pushFront(8.8);
  arr.print();

  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if copy constructor works \n\n";
  cout << "Orginal:  \n";
  arr.print();

  cout << "Copy:  \n";
  Vector<float> copy(arr);
  copy.print();

  cout << "Test: Check if copy assignment operator \n\n";
  cout << "Assignment:  \n";
  Vector<float> temp = arr;
  temp.print();

  cout << "Orginal:  \n";
  arr.print();
}