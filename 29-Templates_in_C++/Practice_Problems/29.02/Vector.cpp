#include "Vector.hxx"

template <typename T>
Vector<T>::Vector() : _size(32), _numOfElements(0) {
  _p_arr = new T[_size]();
}

template <typename T>
Vector<T>::Vector(int size) : _size(size), _numOfElements(0) {
  if (size <= 0) {
    std::cout << "\n Error: The size of your array is negative or equals zero! "
                 "\n I'will set the array size to two \n\n";
    _size = 2;
  }
  _p_arr = new T[_size]();
}

template <typename T>
Vector<T>::~Vector() {
  delete[] _p_arr;
  _p_arr = NULL;
}

template <typename T>
Vector<T>::Vector(const Vector& other)
    : _p_arr(nullptr),
      _size(other._size),
      _numOfElements(other._numOfElements) {
  _p_arr = new T[_size]();
  for (int i = 0; i < _size; i++)
    _p_arr[i] = other._p_arr[i];
}

template <typename T>
Vector<T>& Vector<T>::operator=(const Vector& other) {
  if (this != &other) {
    _size = other._size;
    _numOfElements = other._numOfElements;

    // free memory
    delete[] _p_arr;
    _p_arr = NULL;

    _p_arr = new T[_size]();
    for (int i = 0; i < _size; i++)
      _p_arr[i] = other._p_arr[i];
  }
  return *this;
}

template <typename T>
void Vector<T>::pushFront(const T value) {
  if (_size <= _numOfElements)
    _p_arr = growArray();

  T* _p_temp = new T[_size];
  _p_temp[0] = value;
  for (int i = 1; i < _size; i++)
    _p_temp[i] = _p_arr[i - 1];
  _p_arr = _p_temp;
  _numOfElements++;
}

template <typename T>
void Vector<T>::pushBack(const T value) {
  if (_size <= _numOfElements)
    _p_arr = growArray();
  _p_arr[_numOfElements++] = value;
}

template <typename T>
void Vector<T>::print() {
  for (int i = 0; i < _size; i++) {
    std::cout << " " << i << ": " << get(i) << std::endl;
  }
  std::cout << std::endl;
}

template <typename T>
T* Vector<T>::growArray() {
  _size *= 2;
  T* p_new_array = new T[_size];
  for (int i = 0; i < _size / 2; i++)
    p_new_array[i] = get(i);
  return p_new_array;
}

// Setters
template <typename T>
void Vector<T>::set(const int index, const T value) {
  if (index < 0 || index > _size || index > _numOfElements)
    std::cout << "\n Error: out of range \n\n";
  else
    _p_arr[index] = value;
}

// Getters
template <typename T>
T Vector<T>::get(const int index) {
  return _p_arr[index];
}

template <typename T>
int Vector<T>::getNumOfElements() {
  return _numOfElements;
}

template <typename T>
int Vector<T>::getSize() {
  return _size;
}