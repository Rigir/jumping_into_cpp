/*
    Practise Problem
        3. Implement a logging method that an interface class, StringConvertable
   , with a method toString that converts the resulting object into a string
   representation of itself. The logging method should print out the date and
   time as well as the object itself. (You can find information on getting the
   date at http://www.cplusplus.com/reference/clibrary/ctime/). Again notice how
   we are able to reuse our logging method simply by implementing an interface.
*/

#include <iostream>
#include "SongRekord.h"

using namespace std;

// The program starts here
int main() {
  SongRekord songRekord("Orchid", "Kupla", "Lofi", 137);
  cout << songRekord.toString() << endl;
}
