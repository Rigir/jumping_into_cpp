#ifndef SONGRECKORD_H
#define SONGRECKORD_H

#include "StringConvertable.h"

using namespace std;

class SongRekord : public StringConvertable {
 public:
  SongRekord(string title, string author, string genre, int length);
  string toString();

 private:
  string _title;
  string _author;
  string _genre;
  int _length;
};

#endif  // SONGRECKORD_H