#include "HighScoreElement.h"

HighScoreElement::HighScoreElement(int score, string name)
    : _score(score), _name(name) {}

int HighScoreElement::compare(Comparable& other) {
  int otherScore = dynamic_cast<HighScoreElement&>(other).getScore();
  string otherName = dynamic_cast<HighScoreElement&>(other).getName();

  if (this->_score == otherScore) {
    if (this->_name == otherName)
      return 0;
    else if (this->_name > otherName)
      return 1;
  } else if (this->_score > otherScore)
    return 1;
  return -1;
}

int HighScoreElement::getScore() {
  return this->_score;
}

string HighScoreElement::getName() {
  return this->_name;
}
