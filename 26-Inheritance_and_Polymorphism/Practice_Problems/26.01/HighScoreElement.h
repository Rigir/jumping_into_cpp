#ifndef HIGHSCOREELEMENT_H
#define HIGHSCOREELEMENT_H

#include <string>
#include "Comparable.h"

using namespace std;

class HighScoreElement : public Comparable {
 public:
  HighScoreElement(int score, string name);
  int compare(Comparable& other);

  int getScore();
  string getName();

 private:
  int _score;
  string _name;
};

#endif  // HIGHSCOREELEMENT_H