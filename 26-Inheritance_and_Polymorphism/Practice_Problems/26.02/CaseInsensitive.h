#ifndef CASEINSENSITIVE_H
#define CASEINSENSITIVE_H

#include <string>
#include "Comparator.h"

class CaseInsensitive : public Comparator {
 public:
  int compare(const string& lhs, const string& rhs);

 private:
  string toLowerCase(string str);
};

#endif  // CASEINSENSITIVE_H