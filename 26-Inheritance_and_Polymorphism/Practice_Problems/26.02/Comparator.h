#ifndef COMPARATOR_H
#define COMPARATOR_H

#include <string>

using namespace std;

class Comparator {
 public:
  virtual int compare(const string& lhs, const string& rhs) = 0;
};

#endif  // COMPARATOR_H