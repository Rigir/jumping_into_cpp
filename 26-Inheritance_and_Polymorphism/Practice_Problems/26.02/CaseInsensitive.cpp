#include "CaseInsensitive.h"

#include <iostream>

int CaseInsensitive::compare(const string& lhs, const string& rhs) {
  string lhsLowerCased = toLowerCase(lhs);
  string rhsLowerCased = toLowerCase(rhs);
  if (lhsLowerCased == rhsLowerCased)
    return 0;
  else if (lhsLowerCased > rhsLowerCased)
    return 1;
  else
    return -1;
}

string CaseInsensitive::toLowerCase(string str) {
  for (int i = 0; str[i] != '\0'; i++)
    if (str[i] >= 'A' && str[i] <= 'Z')
      str[i] = str[i] + 32;
  return str;
}