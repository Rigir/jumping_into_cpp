/* 
    Practise Problem
        3. Write a recursive algorithm to remove elements from a linked list. Write a recursive algorithm to
           add elements into a linked list. Try writing the same algorithms using iteration. Do the recursive
           implementations or the iterative implementations feel more natural?
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node {
	int number;
	node *p_next;
}; 

//Global variable
node *p_list = NULL;

//Function headers
void srandSeed();
int randRange (int low, int high);
void display(node* p_head);
node *insertNode(node *p_head, int num);
node *deleteNode(node *start, int num);

//The program starts here
int main(){
    srandSeed();

    //Adding five random nodes
    for (int i = 0; i < 5; i++)
        p_list = insertNode(p_list, randRange (1, 9));
    display(p_list);

    //Delete random node
    p_list = deleteNode(p_list, randRange (1, 5));
    display(p_list);

    delete p_list;
    p_list = NULL;
}

void display(node* p_head){
    if(p_head != NULL){
        cout<< p_head->p_next <<" | "<< p_head->number<<endl;
        display(p_head->p_next);
    }
}

node *deleteNode(node *p_head, int num){ 
    // If invalid num
    if (num < 1) 
       return p_head; 

    // If linked list is empty
    if (p_head == NULL) 
       return NULL; 
 
    // Base case (start needs to be deleted)
    if (num == 1) { 
        node *temp = p_head->p_next; 
        delete(p_head); 
        return temp;   
    } 
      
    p_head->p_next = deleteNode(p_head->p_next, num-1); 
    return p_head; 
} 

node *insertNode(node *p_head, int num){
    if(!p_head)
	    p_head = new node {num, NULL};
	else
        p_head -> p_next = insertNode(p_head -> p_next, num);
	return p_head;
}

int randRange (int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}
