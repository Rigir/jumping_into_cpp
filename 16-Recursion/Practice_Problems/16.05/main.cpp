/* 
    Practise Problem
        5. Write a recursive algorithm to solve the towers of Hanoi problem.
*/

#include <iostream>

using namespace std;

//Function headers
void solveHanoi(int disk, char source, char dest, char sqare);

//The program starts here
int main()
{
    int n;

    cout << "Enter no. of disks:";
    cin >> n;
    solveHanoi(n, 'A', 'B', 'C');

    return 0;
}

void solveHanoi(int disk, char source, char dest, char sqare)
{
    if (disk == 1)
    {
        cout << "Move Disk " << disk << " from " << source << " to " << sqare << endl;
        return;
    }

    solveHanoi(disk - 1, source, sqare, dest);
    cout << "Move Disk " << disk << " from " << source << " to " << sqare << endl;
    solveHanoi(disk - 1, dest, source, sqare);
}