/* 
    Practise Problem
        4. Write a recursive function that takes a sorted array and a target element and finds that element
           in the array (returning the index, or -1 if the element isn’t in the array). How fast can you make
           this search? Can you do better than looking having to look at every element?.
*/

#include <iostream>

using namespace std;

//Function headers
int findElement(int* arr, int size, int index, int element);

//The program starts here
int main(){
    int arr[9] = {2,4,8,16,32,64,128,256,512};
    cout<<findElement(arr, 9, 0, 16);
}

int findElement(int* arr, int size, int index, int element){
    cout<<index<<endl;
    if(element == arr[index]) {
        return arr[index];
    }
    else if ((index > size) || (arr[index] > element)) {
        return -1;
    }
    else{
        if(arr[(size-1)/2] > element)
            return findElement(arr, size, index+1, element);
        else return findElement(arr, size, index + ((size-1)/2), element);
    }
}

