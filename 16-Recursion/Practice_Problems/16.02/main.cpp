/* 
    Practise Problem
        2. Write a recursive function that takes an array and displays the elements in reverse order
           without starting the index of the array at the end. (In other words, don’t write the equivalent of
           a loop that starts printing at the end of the array.)
*/

#include <iostream>

using namespace std;

//Function headers
void reverseArray(int *arr, int size, int index);

//The program starts here
int main(){
    int arr[9] = {2,4,8,16,32,64,128,256,512};

    reverseArray(arr, 9, 0);
}

void reverseArray(int *arr, int size, int index){
    if(index < size)
        reverseArray(arr, size, index + 1);
    cout << arr[index] << endl;
}
