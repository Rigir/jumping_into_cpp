#include "tic-tac-toe.h"

void TicTacToe::playersMove(char player){
    int move;

    while(!(move >= 1 && move <= 9 && BoardSquers[move-1] == ' ')){
        cout<<"\nPlayer "<< player <<" Your turn: ";
            cin>>move;
        
        //error messages
        if(!(move >= 1 && move <= 9))
            cout<<"\n Error: This square doesn't exist! Try again! \n";
        else if(BoardSquers[move-1] != ' ')
            cout<<"\n Error: Your opponent choose this option before! Try again! \n";
    }  
    BoardSquers[move-1] = player;
}  

bool TicTacToe::winner(char player){
    if(checkWinner(player)){
        displayBoard();
        cout<<"\n Player: "<< player << " wins! \n\n";
        return true;
    }
    return false;  
}

bool TicTacToe::checkWinner(char player){
    bool win = false;

    //Columns
    for(int i=0; i<=2; i++)
        if(( BoardSquers[i] == player) && (BoardSquers[i+3] == player) && (BoardSquers[i+6] == player)) win = true;

    //Rows
    for(int i=0; i<=6; i+=3)
        if(( BoardSquers[i] == player) && (BoardSquers[i+1] == player) && (BoardSquers[i+2] == player)) win = true;

    //Diagonals
    if(( BoardSquers[0] == player) && (BoardSquers[4] == player) && (BoardSquers[8] == player)) win = true;
    if(( BoardSquers[2] == player) && (BoardSquers[4] == player) && (BoardSquers[6] == player)) win = true;

    return win;
}

bool TicTacToe::draw(){
    for(int i=0; i<9; i++)
        if(BoardSquers[i] == ' ') return false;
    displayBoard();
    cout<<"\n    DRAW   \n\n";
    return true;
}

void TicTacToe::displayBoard(){
    cout<<"\n";
    for(int i=1; i<=9; i++){
        cout<<" "<< BoardSquers[i-1] << " ";
        if(i % 3)
            cout<<"|";
        else if(i != 9)
            cout<<"\n---+---+---\n";
        else cout<<endl;
    }
}

void TicTacToe::clearBoard(){
    for(int i=0; i<9; i++) BoardSquers[i] = ' ';
}

