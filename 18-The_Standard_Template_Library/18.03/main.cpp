/* 
    Practise Problem
        3. Write a program with two options: register user and log in. Register user allows a new user to
           create a login name and password. Log in allows a user to log in and access a second area, with
           options for “change password” and “log out”. Change password allows the user to change the
           password, and log out will return the user to the original screen.
*/

#include <iostream>
#include <string>
#include <map>

using namespace std;

//Function headers
void logIn(map<string, string> &users);
void registerUser(map<string, string> &users);

//The program starts here
int main(){
    map<string, string> users;
    int menu_choice;

     while(true){
        cout<<"\n| 1 - Login In | 2 - Register | 3 - Exit |\n";
        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
        switch(menu_choice){
            case 1:        
                logIn(users);
                break;
            case 2:        
                registerUser(users);
                break;
            case 3: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout<<" Error: Bad input! Try again!\n";
                break;
        }
    }
}

void logIn(map<string, string> &users){
    map<string, string>::iterator itr;
    string user, password; 

    while(true){
        cout<<"\n Please, enter:\n Username: ";
            cin>>user;
        cout<<" Password: ";  
            cin>>password;

        itr = users.find(user);
        if(itr != users.end() && itr->second == password){
            int menu_choice;
            while(true){
                cout<<"\n| 1 - Change password  | 2 - Log out |\n";
                cout<<"\n Please, enter your choise: ";
                    cin>>menu_choice;
                switch(menu_choice){
                    case 1:  
                        cout<<" New password: "; 
                            cin>>password;      
                        users[user] = password;
                        break;
                    case 2: 
                        cout<<" See you later alligator!\n";
                        return;
                    default:            
                        cout<<" Error: Bad input! Try again!\n";
                        break;
                }
            }
        }
        else{
            cout<<" Error: Login Failed!\n";
        }
    }
}


void registerUser(map<string, string> &users){
    map<string, string>::iterator itr;
    string user, password; 
    while(true){
        cout<<"\n Please, enter:\n Username: ";
            cin>>user;
        cout<<" Password: ";  
            cin>>password;

        itr = users.find(user);
        if(itr != users.end())
            cout<<" The user: ' " << itr->first << " ' already exist.\n";
        else{
            users[user] = password;
            break;
        }
    }
}