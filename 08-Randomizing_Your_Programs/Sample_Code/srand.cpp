/*
	Sample Code
		from: https://www.cprogramming.com/c++book/
*/

#include <cstdlib>
#include <ctime>

int main ()
{
	// call just once, at the very start
	srand( time( NULL ) );
}
