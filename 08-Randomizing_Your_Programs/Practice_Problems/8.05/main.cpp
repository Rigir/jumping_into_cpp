/*
    Practice Problem
        5. Write a program to play poker! You can provide 5 cards to the player, let that player choose new
           cards, and then determine how good the hand is. Think about whether this is easy to do? What
           problems might you have in terms of keeping track of cards that have been drawn already? Was this
           easier or harder than the slot machine?
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>

using namespace std;

//Global variables
enum cardsValues { value, suit, both };
enum databaseTable { Cards, Discard};
enum databaseAction { clear, read , write, played};
string C1,C2,C3,C4,C5,C1D,C2D,C3D,C4D,C5D,result;
int counter;

//Function Heders
void srandSeed();
int randRange(int low, int high);
void displayCards();
void playersMove();
bool checkCardValue(int number);
bool checkCardEqual(int number1, int number2);
bool checkCardExist(string card, int action);
string handRanking(string result);
string drawCard(string card);
string database(int table, int choice, int action);
string databaseOption(string value, int action);
bool flush();
bool onePair();
bool twoPair();
bool fourOfAKind();
bool fullHouse();
bool threeOfAKind();
bool straight(int i);


//The program starts here!
int main(){
    srandSeed();
    for(int i=1; i<=5; i++){
        database(Cards,i,clear);
        database(Discard,i,clear);
    }
    for(int i=1; i<=5; i++) database(Cards,i,write);
    displayCards();
    playersMove();
    displayCards();
}

void playersMove(){
    int choice;

    cout<<" Which cards would you like to exchange (1-5)? \n If you want to keep your cards press '0' \n";

	for(int i=1; i<=5; i++){
	    do{
            cout<<" Choice the card: ";
            cin>>choice;

		    //error messages
		    if(!(choice >= 0 && choice <= 5)) cout<<"_This card doesn't exist! Try again! \n";
		    else if(database(Discard, choice, read) != " " && choice != 0) cout<<"_You discard this card! Try again! \n";
            
	    }while(!(choice >= 0 && choice <= 5) || choice != 0  && (database(Discard, choice, read) != " ") );
	    if( choice == 0 ) break;  
	    database(Cards, choice, write);
	    database(Discard, choice, played);
	}
}

void displayCards(){
    cout<<"\n Your Hend Strength: "<< handRanking(result) <<endl;

    for(int i=1; i<=5; i++) cout<<" | "<<database(Cards, i, read)<<" |";
    cout<<"\n\n";
}

string handRanking(string result){
    for(int i=1; i<=9; i++){
        if(straight(i) && flush() && (i == 1)) return result = "Royal Flush";
        if(straight(i) && flush()) return result = "Straight Flush";
        if(straight(i)) return result = "Straight";
    }
    if(fourOfAKind()) return result = "4 Of A Kind";
    if(fullHouse()) return result = "Full House";
    if(flush()) return result = "Flush";
    if(threeOfAKind()) return result = "3 Of A Kind";
    if(twoPair()) return result = "2 Pair";
    if(onePair()) return result = "1 Pair";
    else return result = "High Card";
}

bool straight(int i){
    if(checkCardValue(i) && checkCardValue(i+1) && checkCardValue(i+2) && checkCardValue(i+3) && checkCardValue(i+4)) return true;
    return false;
}

bool fourOfAKind(){
    for(int i=1; i<=5; i++)
        for(int j=1; j<=5; j++)
            for(int k=1; k<=5; k++)
                for(int l=1; l<=5; l++)
                if((i != l) && (i != k) && (i != j) && (j != k) && (j != l) && (k != l))
                    if(checkCardEqual(i,j) && checkCardEqual(i,k) && checkCardEqual(i,l) && checkCardEqual(j,k) && checkCardEqual(j,l) && checkCardEqual(k,l))
                        return true;
    return false;
}

bool fullHouse(){
    for(int i=1; i<=5; i++)
        for(int j=1; j<=5; j++)
            for(int k=1; k<=5; k++)
                if((i != k) && (i != j) && (k != j))
                    if(checkCardEqual(i,j) && checkCardEqual(i,k) && checkCardEqual(k,j))
                        for(int l=1; l<=5; l++)
                            for(int m=1; m<=5; m++)
                                if((l != i) && (l != j) && (l != k) && (m != i) && (k != j) && (l != k) && (l != m))
                                    if(checkCardEqual(l,m))
                                        return true;
    return false;
}

bool flush(){
    counter=0;
    for(int i=1; i<=5; i++)
        if( database(Cards, i, read)[1]  == database(Cards, 1, read)[1] ) counter++;
    if(counter == 5) return true;
    return false;
}

bool threeOfAKind(){
    for(int i=1; i<=5; i++)
        for(int j=1; j<=5; j++)
            for(int k=1; k<=5; k++)
                if((i != k) && (i != j) && (k != j))
                    if(checkCardEqual(i,j) && checkCardEqual(i,k) && checkCardEqual(k,j))
                        return true;
    return false;
}

bool twoPair(){
    for(int i=1; i<=5; i++)
        for(int j=1; j<=5; j++)
            if(checkCardEqual(i,j) && (i != j))
                for(int k=1; k<=5; k++)
                    for(int l=1; l<=5; l++)
                        if((k != i) && (k != j) && (l != i) && (l != j) && (k != l))
                            if(checkCardEqual(k,l)) 
                                return true;
    return false;
}

bool onePair(){
    for(int i=1; i<=5; i++)
        for(int j=1; j<=5; j++)
            if(checkCardEqual(i,j) && (i != j)) return true;
    return false;
}

bool checkCardEqual(int number1, int number2){
    if((database(Cards, number1, read)[0] == database(Cards, number2, read)[0])) return true;
    return false;
}

bool checkCardValue(int number){
   switch(number){
        case 1: 
            if(checkCardExist("A",value)) return true;
            return false;
        case 2: 
            if(checkCardExist("K",value)) return true;
            return false;
        case 3: 
            if(checkCardExist("Q",value)) return true;
            return false;
        case 4: 
            if(checkCardExist("J",value)) return true;
            return false;
        case 5:  
            if(checkCardExist("T",value)) return true;
            return false;
        case 6:  
            if(checkCardExist("9",value)) return true;
            return false;
        case 7:  
            if(checkCardExist("8",value)) return true;
            return false;
        case 8: 
            if(checkCardExist("7",value)) return true;
            return false;
        case 9:  
            if(checkCardExist("6",value)) return true;
            return false;
        case 10:  
            if(checkCardExist("5",value)) return true;
            return false;
        case 11:  
            if(checkCardExist("4",value)) return true;
            return false;
        case 12:  
            if(checkCardExist("3",value)) return true;
            return false;
        case 13:  
            if(checkCardExist("2",value)) return true;
            return false;
        default: return false;
    }
}

bool checkCardExist(string card, int action){
    switch (action){
        case both:
            for(int i=1; i<=5; i++)
                if(database(Cards, i, read) == card) return true;
            return false;
        case value:
            for(int i=1; i<=5; i++)
                if(database(Cards, i, read)[0] == card[0]) return true;
            return false;
        default: return false;
    }
}

string drawCard(string card){
    int suit = randRange(1,4), rank = randRange(1,13);
    card = databaseOption(card, clear);

    switch(rank){
        case 1:  card = "A"; break; //Ace
        case 2:  card = "2"; break;
        case 3:  card = "3"; break;
        case 4:  card = "4"; break;
        case 5:  card = "5"; break;
        case 6:  card = "6"; break;
        case 7:  card = "7"; break;
        case 8:  card = "8"; break;
        case 9:  card = "9"; break;
        case 10: card = "T"; break; //Ten
        case 11: card = "J"; break; //Jack
        case 12: card = "Q"; break; //Queen
        case 13: card = "K"; break; //King
        default: card = "!"; break; 
    }

    switch(suit){
        case 1: return card += "S"; //Spades 
        case 2: return card += "H"; //Hearts 
        case 3: return card += "C"; //Clubs 
        case 4: return card += "D"; //Diamonds
        default: return card += "!";
    }
}

string database(int table, int choice, int action){
    switch (table){
        case Cards:
            switch(choice){
                case 1:
                    return C1 = databaseOption(C1,action);
                case 2:
                    return C2 = databaseOption(C2,action);
                case 3:
                    return C3 = databaseOption(C3,action);
                case 4:
                    return C4 = databaseOption(C4,action);
                case 5:
                    return C5 = databaseOption(C5,action);
                default: return "404 - Card";
            }
        case Discard:
            switch(choice){
                case 1:
                    return C1D = databaseOption(C1D,action);
                case 2:
                    return C2D = databaseOption(C2D,action);
                case 3:
                    return C3D = databaseOption(C3D,action);
                case 4:
                    return C4D = databaseOption(C4D,action);
                case 5:
                    return C5D = databaseOption(C5D,action);
                default: return "404 - Discard";
            }
        default: return "404 - Table";
    }
}

string databaseOption(string value, int action){
    switch(action){
        case read: return value;
        case clear: return value = " ";
        case played: return value = "D";
        case write: 
            while(checkCardExist(value, both)) value = drawCard(value);
            return value;
        default: return "404 - DB_Option";
    }
}

int randRange(int low, int high){
    return rand() % ( high - low +  1) + low;
}

void srandSeed(){
    int seed = time(NULL);
    cout<<" srandSeed: "<<seed<<endl;
    srand(seed);
}
