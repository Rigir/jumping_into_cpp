/*
    Practice Problem
        1. Write a program that simulates a coin flip. Run it many times—do the results look random to you?
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Headers
void srandSeed();
void coinFlip();
int randRange(int low, int high);


//The program starts here!
int main (){
    srandSeed();
    coinFlip();  
}

void coinFlip(){
    if( randRange(1,2) == 1 )
        cout<<"\n Heads \n";
    else
        cout<<"\n Tails \n";
}

int randRange (int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}