/* 
    Practise Problem
        3. Take the implementation of binary trees that you used in for the exercises in the binary trees
           chapter and move all function declarations and structure declarations into a header file. Put the
           structure declarations in one file and the function declarations into another file. Move all the
           implementation into one source file. Create a small program that exercises the basic
           functionality of the of binary tree.
*/

#include "binary_tree.h"

//The program starts here
int main(){{
    int choice = 0;
    int node_value = 0;
    node *p_root = NULL;

    while (true){
	    cout << "What would you like to do?\n\n1. Add a node\n2. Remove a node\n3. Destroy the tree\n4. Check if a node is in the tree\n5. Exit the program\n";
	    cin >> choice;
        switch (choice){
            case 1:
                cout << "Please enter a value to insert: ";
                    cin >> node_value;
                p_root = insert( p_root, node_value );
                cout << "\nAdded value " << node_value << " to tree\n\n";
                break;
            case 2:
                cout << "Please enter a value to remove: ";
                    cin >> node_value;
                p_root = remove( p_root, node_value );
                cout << "\nRemoved value " << node_value << " from tree\n\n";
                break;
            case 3:
                destroy_tree( p_root );
                p_root = NULL;
                cout << "\nDestroyed tree\n\n";
                break;
            case 4:{
                cout << "Please enter a value to find: ";
                    cin >> node_value;
                node* p_search_node = search( p_root, node_value );
                if ( p_search_node != NULL ){
                    cout << "\nFound node\n\n";
                }
                else{
                    cout << "\nNode not found\n\n";
                }
                break;
            }
            case 5:
                return 0;
            default: cout << "Bad input...\n\n";
        }
    }
}
}
