#include <iostream>

using namespace std;

struct node{
	int key_value;
	node *p_left;
	node *p_right;
};

node* insert (node *p_tree, int key);
node *search (node *p_tree, int key);
node* remove (node* p_tree, int key);
void destroy_tree (node *p_tree);