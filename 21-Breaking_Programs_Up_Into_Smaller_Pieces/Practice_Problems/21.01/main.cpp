/* 
    Practise Problem
        1. Write a program that contains the functions add, subtract, multiply and divide. Each of
           these functions should take two integers, and return the result of the operation. Create a small
           calculator in that uses these functions. Put the function declarations into a header file, but
           include the source code for these functions in your source file.
*/

//#define DEBUG
#include <iostream>
#include "calc_operations.h"

using namespace std;

//The program starts here
int main(){
  char arithmetic_operations;
  int number_one, number_two;

    cout<<"\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;

    if(arithmetic_operations == '+'){
        cout<<"\n Result: "<< addition(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '-'){
        cout<<"\n Result: "<< subtraction(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '/' || arithmetic_operations == '%'){
        cout<<"\n Result: "<< division(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '*' || arithmetic_operations == 'x'){
        cout<<"\n Result: "<< multiplication(number_one,number_two) <<endl;
    }

    #ifdef DEBUG
        cout <<"\n == BEBUG ===\n Variable n1: "<< number_one <<"\n Variable n1: "<< number_two <<"\n Arithmatic Operator : "<< arithmetic_operations  ;
    #endif
}

int addition(const int& x, const int& y){
    return x + y;
}

int subtraction(const int& x, const int& y){
    return x - y;
}

int division(const int& x, const int& y){
    return x / y;
}

int multiplication(const int& x, const int& y){
    return x * y;
}