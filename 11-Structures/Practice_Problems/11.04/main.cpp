/* 
    Practise Problem
        1. Write a program that allows a user to enter high scores of a game, keeping tracking of the name
           of the user and the score. Add the ability to show the highest score for each user, all scores for a
           particular user, all scores from all users, and the list of users.
*/

#include <iostream>
#include <string>

using namespace std;

//Structures
struct player{
    string nickName = " ";
    int highestScore = 0;
    int scores[20];
};

//Function headers
void displayMenu(player players[]);
void showPlayers(player players[]);
void showPlayerScores(player players[]);
void showAllPlayersScores(player players[]);
void addNewPlayerOrScore(player players[]);
void addScore(player players[],int player);
void clearScors(player players[],int player);
bool checkPlayerExists(player players[], string nickName);

//The program starts here
int main(){
    player players[10];
    int menu_choice;

    while (true){
        displayMenu(players);
        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
         switch(menu_choice){
            case 1:        
                addNewPlayerOrScore(players);
                break;
            case 2:
                showAllPlayersScores(players);
                break;
            case 3:
                showPlayerScores(players);
                break;
            case 4:
                showPlayers(players);
                break;
            case 5: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout << " Error: Bad input! Try again!\n";
                break;
        }
    }
}

void showPlayers(player players[]){
    cout<<"\n| --------------- All PLAYERS --------------- |\n\n";
    for(int i=0; i<10; i++){
        if(players[i].nickName != " ")
            cout<<i+1<<": "<<players[i].nickName<<endl;
    }
}

void showAllPlayersScores(player players[]){
    cout<<"\n| ------------- PLAYERS SCORES ------------- |\n\n";
    for(int i=0; i<10; i++){
        if(players[i].nickName != " "){
            cout<<i+1<<": "<<players[i].nickName<<"\n Scores: \n";
            for(int j=0; j<20; j++){
                if(players[i].scores[j] != 0)
                    cout<<"\t- "<<players[i].scores[j]<<endl;
            }
            cout<<endl;
        }
    }
}

void showPlayerScores(player players[]){
    string nickName;
    cout<<"\n Please, enter:\n Nick Name of player you looking for: ";
        cin>>nickName;
    cout<<"\n| ------------ Particular player ------------ |\n";
    if(checkPlayerExists(players,nickName)){
        for(int i=0; i<10; i++){
            if(players[i].nickName == nickName){
                cout<<"\n Nick: "<<players[i].nickName<<"\n Scores: \n";
                    for(int j=0; j<20; j++)
                        if(players[i].scores[j] != 0)
                            cout<<"\t- "<<players[i].scores[j]<<endl;
            }
        }
    }
    else cout<<"\n Error: This player doesn't exist! \n";
}


void addNewPlayerOrScore(player players[10]){
    string nickName;
    cout<<"\n Please, enter:\n User Name: ";
        cin>>nickName;

    if(checkPlayerExists(players,nickName)){
        for(int i=0; i<10; i++)
            if(players[i].nickName == nickName){
                addScore(players,i);
                break;
            }
    }
    else{
        for(int i=0; i<10; i++)
            if(players[i].nickName == " "){
                players[i].nickName = nickName;
                clearScors(players,i);
                addScore(players,i);
                break;
            }
    }
}

void addScore(player players[],int player){
    for(int i=0; i<20; i++)
        if(players[player].scores[i] == 0){
            cout<<" Score: ";
                cin>>players[player].scores[i];
            if(players[player].scores[i] > players[player].highestScore)
                    players[player].highestScore = players[player].scores[i];
            break;
        }
}

void clearScors(player players[10],int player){
    for(int i=0; i<20; i++)
        players[player].scores[i] = 0;
}

bool checkPlayerExists(player players[], string nickName){
    for(int i=0; i<10; i++)
        if(nickName == players[i].nickName) return true;
    return false;
}
 
void displayMenu(player players[]){
    cout<<"\n| ----------------------------------------------------------------------------------------------- |\n";
    for(int i=0; i<10; i++){
        if(players[i].nickName != " ")
            cout<<"  "<<i+1<<" Nick: "<<players[i].nickName<<"\t Highest Score: "<<players[i].highestScore<<endl;
    }
    cout<<"\n| 1 - Add New Score |  Show:  2 - All Scores | 3 - Particular player | 4 - All players | 5 - Exit |\n";
}