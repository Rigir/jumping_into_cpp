/* 
    Practise Problem
        2. Create an array of space ship objects and write a program that continually updates their
           positions until they all go off the screen. Assume that the size of the screen is 1024 pixels by 768
           pixels.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Structures
struct spaceShip{
	int x_coordinate;
	int y_coordinate;
};

//Function headers
int randRange(int low, int high);
void srandSeed();
spaceShip getNewShip();
spaceShip updateCoordinate(spaceShip ship);
void displayBoard(spaceShip ships[]);

//The program starts here
int main(){
    srandSeed();
    spaceShip ships[15];
    for(int i=0; i<15; i++){
        ships[i] = getNewShip();
    }
    
    for(int j=0; j<12; j++){
        displayBoard(ships);
        for(int i=0; i<15; i++)
            ships[i] = updateCoordinate(ships[i]);
    }
}

spaceShip getNewShip(){
    spaceShip ship;
	ship.x_coordinate = randRange(0,9);
	ship.y_coordinate = 0;
	return ship;
}

spaceShip updateCoordinate(spaceShip ship){
	ship.y_coordinate += randRange(1,3);
    return ship;
}

void displayBoard(spaceShip ships[]){
    for(int i=0; i<13; i++){
        cout<<"|";
        for(int j=0; j<13; j++){
            for(int k=0; k<15; k++)
            if(i == ships[k].x_coordinate && j == ships[k].y_coordinate)
                cout<<">";
            else 
                cout<<" ";
        }
        cout<<"|"<<endl;
    }
}

int randRange(int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}