/*
    Practice Problem
        3. Write a small calculator that takes as input one of the four arithmetic operations, the two
           arguments to those operations, and then prints out the result
*/

#include <iostream>

using namespace std;

int main(){
  char arithmetic_operations;
  int number_one, number_two;

    cout<<"\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;


    if(arithmetic_operations == '+'){
        cout<<"\n Result: "<< number_one + number_two <<endl;
    }
    else if( arithmetic_operations == '-'){
        cout<<"\n Result: "<< number_one - number_two <<endl;       
    }
    else if( arithmetic_operations == '/' || arithmetic_operations == '%'){
        cout<<"\n Result: "<< number_one / number_two <<endl;
    }
    else if( arithmetic_operations == '*' || arithmetic_operations == 'x'){
        cout<<"\n Result: "<< number_one * number_two <<endl;
    }
}