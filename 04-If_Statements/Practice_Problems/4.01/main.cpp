/* 
    Practise Problem
        1. Ask the user for two users' ages, and indicate who is older; behave differently if both are over 100.
*/

#include <iostream>

using namespace std;

int main(){
  int user1_age, user2_age;
  
    cout<<" I will indicate who is older \n";
    cout<<"\n Please enter first user age: ";
        cin>>user1_age;
    cout<<" Pleace enter secoud user age: ";
        cin>>user2_age;
  
    if( user1_age > user2_age ){
        cout<<"\n The first user is older \n";
    }
    else if( user1_age < user2_age ){
        cout<<"\n The second user is older \n";
    }
    else if( user1_age == user2_age ){
        cout<<"\n You are both have this same age \n";
    }
  
    if( ( user1_age > 100 ) && ( user2_age > 100 )){
        cout<<" It is never too late to Start Programming \n";
    } 
}
