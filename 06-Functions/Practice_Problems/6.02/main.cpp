/*
    Practice Problem
        2. Make your calculator program perform computations in a separate function for each type of
           computation.
*/

#include <iostream>

using namespace std;

//Function headers
int addition(int x, int y);
int subtraction(int x, int y);
int division(int x, int y);
int multiplication(int x, int y);

//The program starts here
int main(){
  char arithmetic_operations;
  int number_one, number_two;

    cout<<"\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;

    if(arithmetic_operations == '+'){
        cout<<"\n Result: "<< addition(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '-'){
        cout<<"\n Result: "<< subtraction(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '/' || arithmetic_operations == '%'){
        cout<<"\n Result: "<< division(number_one,number_two) <<endl;
    }
    else if( arithmetic_operations == '*' || arithmetic_operations == 'x'){
        cout<<"\n Result: "<< multiplication(number_one,number_two) <<endl;
    }
}

int addition(int x, int y){
    return x + y;
}

int subtraction(int x, int y){
    return x - y;
}

int division(int x, int y){
    return x / y;
}

int multiplication(int x, int y){
    return x * y;
}