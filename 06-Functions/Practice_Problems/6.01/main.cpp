/* 
    Practise Problem
        1. Take the "menu program" you wrote earlier and break it out into a series of calls to functions for
           each of the menu items. Add the calculator and "99 bottles of beer" as two different functions 
           that can be called.
*/

#include <iostream>

using namespace std;

//Function headers
void startGame();
void calculator();
void songBottles();
void options();
void exit();

//The program starts here
int main(){
    int menu_choice;
    
    while(!(menu_choice >= 1 && menu_choice <= 5)){
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Calculator \n";
        cout<<" 3. Song: '99 bottles of bear' \n";
        cout<<" 4. Options \n";
        cout<<" 5. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
    }

    if( menu_choice == 1)
        startGame();
    else if( menu_choice == 2)
        calculator();
    else if( menu_choice == 3)
        songBottles();
    else if( menu_choice == 4)
        options();
    else if( menu_choice == 5)
        exit();
}

void startGame(){
    cout<<" Let's Rock! \n";
}

void options(){
    cout<<" Looking for a resolution ? \n";
}

void exit(){
    cout<<" It's now safe to turn off your computer! \n";
}

void songBottles(){
    for(int BottlesNumber = 100; BottlesNumber >= 1;){
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
        BottlesNumber--;

        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }
}

void calculator(){
     char arithmetic_operations;
     int number_one, number_two;

    cout<<"\n\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;


    if(arithmetic_operations == '+'){
        cout<<"\n Result: "<< number_one + number_two <<endl;
    }
    else if( arithmetic_operations == '-'){
        cout<<"\n Result: "<< number_one - number_two <<endl;       
    }
    else if( arithmetic_operations == '/' || arithmetic_operations == '%'){
        cout<<"\n Result: "<< number_one / number_two <<endl;
    }
    else if( arithmetic_operations == '*' || arithmetic_operations == 'x'){
        cout<<"\n Result: "<< number_one * number_two <<endl;
    }
}