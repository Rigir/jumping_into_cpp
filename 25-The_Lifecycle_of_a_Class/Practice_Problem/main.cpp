/*
    Practise Problem
        1. Implement a vector replacement that operates only on integers,
   vectorOfInt (you don't need to use templates like the normal STL). Your class
   should have the following interface:

                • A no-argument constructor that allocates a 32 element vector
                • A constructor that takes an initial size as the argument
                • A method get, taking an index as returning the value at that
   index • A method set, that takes an index and a value, and sets the value at
   that index • A method pushBack that adds an element to the end of the array,
   resizing if necessary • A method pushfront that adds an element to the
   beginning of the array • A Copy constructor and assignment operator

           Your class should not leak memory; any memory it allocates must be
   deleted. Try to think carefully about how your class can be misused, and how
   you should handle them. What do you do if a user gives a negative initial
   size? What about accessing a negative index?
*/

#include "VectorInt.h"

using namespace std;

// The program starts here
int main() {
  cout << "\nTest: Contructor with negative size: \n";
  VectorInt arr(-1);
  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if pushBack scale size \n\n";
  arr.pushBack(2);
  arr.pushBack(9);
  arr.print();

  arr.pushBack(3);
  arr.print();
  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if u can set value out of index  \n\n";
  cout << "Positive num:  \n";
  arr.set(5, 15);
  cout << "Negative num:  \n";
  arr.set(-1, 15);
  cout << "Correct (2,15):  \n";
  arr.set(2, 15);
  arr.print();

  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if pushFront works fine  \n\n";
  arr.pushFront(6);
  arr.print();

  arr.pushFront(8);
  arr.print();

  cout << "Arr.size:" << arr.getSize() << endl;
  cout << "Arr.getNumOfElements:" << arr.getNumOfElements() << "\n\n";

  cout << "Test: Check if copy constructor works \n\n";
  cout << "Orginal:  \n";
  arr.print();

  cout << "Copy:  \n";
  VectorInt copy(arr);
  copy.print();

  cout << "Test: Check if copy assignment operator \n\n";
  cout << "Assignment:  \n";
  VectorInt temp = arr;
  temp.print();

  cout << "Orginal:  \n";
  arr.print();
}
