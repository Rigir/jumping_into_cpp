#include <iostream>

class VectorInt {
 public:
  VectorInt();                                   // constructor
  VectorInt(int size);                           // constructor
  ~VectorInt();                                  // destructor
  VectorInt(const VectorInt& other);             // copy constructor
  VectorInt& operator=(const VectorInt& other);  // assignment operator

  void print();
  void pushFront(const int value);
  void pushBack(const int value);
  // Setters
  void set(const int index, const int value);

  // Getters
  int get(const int index);
  int getNumOfElements();
  int getSize();

 private:
  int* _p_arr;
  int _size, _numOfElements;
  int* growArray();
};
