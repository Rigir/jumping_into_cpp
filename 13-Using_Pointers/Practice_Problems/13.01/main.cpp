/* 
    Practise Problem
        1. Write a function that prompts the user to enter his or her first name and last name, as two separate
           values. This function should return both values to the caller via additional pointer (or reference)
           parameters that are passed to the function. Try doing this first with pointers and then with references.
           (Hint: the function signature will look be similar to the swap function from earlier!)
*/

#include <iostream>
#include <string>

using namespace std;

//Function headers
void usePointer(string *p_firstName, string *p_lastName);
void useReference(string &firstName, string &lastName);

//The program starts here
int main(){
	string firstName, lastName;

	usePointer( &firstName, &lastName);
	cout <<" Result: "<< firstName << " " << lastName << '\n';
	useReference( firstName, lastName);
	cout <<" Result: "<< firstName << " " << lastName << '\n';
}

void usePointer(string *p_firstName, string *p_lastName){
	cout<<"\n POINTER \n Please, enter your first name: ";
		cin>>*p_firstName;
	cout<<" Please, enter your last name: ";
		cin>>*p_lastName;
}

void useReference(string &firstName, string &lastName){
	cout<<"\n REFERENCE \n Please, enter your first name: ";
		cin>>firstName;
	cout<<" Please, enter your last name: ";
		cin>>lastName;
}