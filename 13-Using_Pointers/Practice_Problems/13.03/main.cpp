/* 
    Practise Problem
        3. Modify the program you wrote for exercise 1 so that instead of always prompting the user for a last
           name, it does so only if the caller passes in a NULL pointer for the last name.
*/

#include <iostream>
#include <string>

using namespace std;

//Function headers
void userName(string *p_firstName, string *p_lastName);

//The program starts here
int main(){
	string firstName, lastName;
	
	userName( &firstName, &lastName);
	cout <<" 1. Result (Without null): "<< firstName << " " << lastName << '\n';
	userName( &firstName, NULL);
	cout <<" 2. Result (With null): "<< firstName << " " << lastName << '\n';
}

void userName(string *p_firstName, string *p_lastName){
	cout<<"\n User Name \n Please, enter your first name: ";
		cin>>*p_firstName;
	
	if(p_lastName){ //Same as (p_lastName != NULL)
		cout<<" Please, enter your last name: ";
			cin>>*p_lastName;
	}
}
