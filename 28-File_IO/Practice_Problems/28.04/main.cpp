/*
    Practise Problem
        4. Rewrite your HTML parser so that it uses your XML parser instead of
   the hand-coded parsing you had before. Add support for displaying lists. You
   should be able to read the <ul> tag or the <nl> tag for unordered and
   numbered lists. Each list item should be between <li> and </li> tags. The
   display for <ul> <li>first item</li> <li>second item</li>
            </ul>

            Should be
                * first item
                * second item

            And for
            <nl>
                <li>first item</li>
                <li>second item</li>
            </nl>

            Should be
                1. first item
                2. second item
           Make sure that you restart your numbering if a second numbered list
   appears!
*/

/*
  Solution provided by:
  https://github.com/54skyxenon/Jumping-Into-CPP-Exercises
*/

#include <fstream>
#include <iostream>
#include <vector>
#include "HTML_DOC.h"
#include "HTML_TAG.h"

using namespace std;

// The program starts here
int main() {
  std::ifstream HTML_file("Sample_HTML.txt");
  if (!HTML_file.is_open()) {
    std::cout << "Could not open file!" << std::endl;
    return -1;
  }

  std::string parsedTagOrText;
  std::string raw_HTML = "";

  while (HTML_file >> parsedTagOrText)
    raw_HTML += parsedTagOrText + " ";

  HTML_file.close();
  HTML_DOC myHTMLDoc(raw_HTML);
  myHTMLDoc.parseTags();
}
