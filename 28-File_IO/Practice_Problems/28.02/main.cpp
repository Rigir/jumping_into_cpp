/*
    Practise Problem
        2. Modify the HTML parser you implemented in Chapter 19: More about
   Strings so that it can read data from a file on disk.
*/

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

void readHTML(vector<string>&, const string&);
void formatHTML(vector<string>&);

int main() {
  vector<string> tags;
  string input, temp;

  ifstream file("main.txt");

  while (file >> temp)
    input += temp + " ";

  readHTML(tags, input);
  formatHTML(tags);
}

void readHTML(vector<string>& tags, const string& html) {
  for (int i = 0; i < html.length(); ++i) {
    if (html[i] == '<' || html[i] == '/') {
      ++i;
      if (html[i] == '/') {
        ++i;
      }

      string temp_tag = "";
      while (html[i] != '>') {
        temp_tag += html[i++];
      }
      tags.push_back(temp_tag);
    } else {
      string temp_text;
      while (html[i] != '<') {
        temp_text += html[i++];
      }
      tags.push_back(temp_text);
    }
  }
}

void formatHTML(vector<string>& tags) {
  for (int i = 0; i < tags.size(); i++) {
    if ((tags[i]) == "b") {
      string& textContent = tags[++i];
      textContent.insert(textContent.begin(), '*');
      textContent.push_back('*');
      cout << textContent << endl;
      ++i;
    } else if ((tags[i]) == "i") {
      string& textContent = tags[++i];
      textContent.insert(textContent.begin(), '_');
      textContent.push_back('_');
      cout << textContent << endl;
      ++i;
    } else if (((tags[i])[0]) == 'a') {
      string url = (tags[i]).substr(8);
      url.insert(url.begin(), '(');
      url.resize(url.size() - 1);
      url.push_back(')');
      string& textContent = tags[++i];
      textContent += " " + url;
      cout << textContent << endl;
      ++i;
    }
  }
}
