/*
    Practise Problem
        1. Reimplement the text file version of the high-score program that
   inserts into the correct file position, but do it using a binary file format
   instead of a text file format. How can you tell if your program is working?
   Create a program that displays the file as a text file.
*/

/*
  Solution provided by:
  https://github.com/54skyxenon/Jumping-Into-CPP-Exercises
*/

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  fstream file("highscores.bin", ios::in | ios::out | ios::binary);

  if (!file.is_open()) {
    cout << "Could not open file!" << '\n';
    return 0;
  }

  int new_high_score;
  cout << "Enter a new high score: ";
  cin >> new_high_score;

  streampos pre_score_pos = file.tellg();
  int cur_score;

  while (file.read(reinterpret_cast<char*>(&cur_score), sizeof(cur_score))) {
    if (cur_score <= new_high_score)
      break;
    pre_score_pos = file.tellg();
  }

  if (!file.eof() && file.fail()) {
    cout << "Bad score read -- exiting" << endl;
    file.close();
    return -1;
  }

  file.clear();
  file.seekg(pre_score_pos);

  vector<int> myRemainingScores(0);
  int currentScore;

  while (
      file.read(reinterpret_cast<char*>(&currentScore), sizeof(currentScore)))
    myRemainingScores.push_back(currentScore);

  if (!file.eof() && file.fail()) {
    cout << "Bad score read -- exiting" << endl;
    file.close();
    return -1;
  }

  file.clear();
  file.seekp(pre_score_pos);

  file.write(reinterpret_cast<char*>(&new_high_score), sizeof(new_high_score));

  for (unsigned int i = 0; i < myRemainingScores.size(); i++)
    file.write(reinterpret_cast<char*>(&myRemainingScores[i]),
               sizeof(myRemainingScores[i]));

  file.seekg(0, ios_base::beg);
  int scoreRead;

  while (file.read(reinterpret_cast<char*>(&scoreRead), sizeof(scoreRead)))
    cout << endl << scoreRead;

  if (!file.eof() && file.fail()) {
    cout << "Bad score read -- exiting" << endl;
    file.close();
    return -1;
  }

  file.clear();
  file.close();
}
