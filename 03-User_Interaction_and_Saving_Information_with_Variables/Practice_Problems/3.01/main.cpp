/* 
    Practise Problem
        1. Write a program that outputs your name.
*/

#include <iostream>
#include <string>

using namespace std;

int main(){
  string user_name; 
 
    cout<<"Please enter your last name: ";
        cin>>user_name;

    cout<<"Your name is: "<<user_name<<endl;  	
}
