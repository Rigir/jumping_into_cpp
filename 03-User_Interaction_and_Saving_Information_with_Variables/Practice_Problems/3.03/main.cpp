/* 
    Practise Problem
    3. Write a program that performs division of two numbers read from the user and prints out anexact result. 
       Make sure to test your program with both integer inputs and decimal inputs.
*/

#include <iostream>

using namespace std;

int main(){
  float a,b;

    cout<<"I will division your two numbers \n";
    cout<<" First Number: ";
        cin>>a;
    cout<<" Second Number: ";
        cin>>b;
    cout<<"The division of "<< a <<" and "<< b <<" is "<< a/b <<endl;
}
