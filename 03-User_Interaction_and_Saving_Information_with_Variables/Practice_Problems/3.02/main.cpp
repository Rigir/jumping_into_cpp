/* 
    Practise Problem
        2. Write a program that reads in two numbers and adds them together.
*/

#include <iostream>

using namespace std;

int main(){
  int a,b;

    cout<<"I will sum your two numbers \n";
    cout<<" First Number: ";
        cin>>a;
    cout<<" Second Number: ";
        cin>>b;
    cout<<"\n The sum of "<< a <<" and "<< b <<" is "<< a+b <<endl;
}
