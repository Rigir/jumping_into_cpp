/*
    Practise Problem
        6. Implement a simple map, in the form of a binary tree, that holds an
   address book; the key for the map should be a person's name and the value
   should be the person's email address. You should provide the ability to add
   email addresses to the map, remove email addresses, update email addresses,
   and of course find email addresses. You'll also want to clean up your address
   book when your program shuts down. As a reminder, you can use any of the
   standard C++ comparison operators (such as ==, < or >) to compare two
   strings.
*/

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

// Structures
struct node {
  string key;
  string value;
  node* p_left;
  node* p_right;
};

void displayMenu(node* p_tree);
node* addNewPerson(node* p_tree);
void findPerson(node* p_tree);
node* updatePersonEmail(node* p_tree);
node* deletePerson(node* p_tree);

void displayTree(node* p_tree);
void destroyTree(node* p_tree);
node* find_max(node* p_tree);
node* insert(node* p_tree, string key, string value);
node* update(node* p_tree, string searched_key, string new_value);
node* search(node* p_tree, string key);
node* remove(node* p_tree, string key);
node* remove_max_node(node* p_tree, node* p_max_node);

// The program starts here
int main() {
  node* p_root = NULL;
  int menu_choice;

  while (true) {
    displayMenu(p_root);
    cout << "\n Please, enter your choise: ";
    cin >> menu_choice;
    switch (menu_choice) {
      case 1:
        p_root = addNewPerson(p_root);
        break;
      case 2:
        findPerson(p_root);
        break;
      case 3:
        p_root = updatePersonEmail(p_root);
        break;
      case 4:
        p_root = deletePerson(p_root);
        break;
      case 5:
        cout << " It's now safe to turn off your computer! \n";
        return true;
      default:
        cout << " Error: Bad input! Try again!\n";
        break;
    }
  }

  destroyTree(p_root);
  p_root = NULL;
}

void displayMenu(node* p_tree) {
  cout << "\n\t\t\t  Address Book \n";
  displayTree(p_tree);
  cout << "\n| 1 - Add contact | 2 - Find contact | 3 - Update contact | 4 - "
          "Remove contact | 5 - Exit |\n";
}

node* addNewPerson(node* p_tree) {
  string name, email;
  cout << "\n Please, enter:\n First Name: ";
  cin >> name;
  cout << " Email: ";
  cin >> email;
  return insert(p_tree, name, email);
}

void findPerson(node* p_tree) {
  string searched_key;
  cout << "\n Enter a first name of person you looking for: ";
  cin >> searched_key;
  node* p_searched = search(p_tree, searched_key);
  if (p_searched != nullptr)
    cout << "\n Entry:" << p_searched->key << " " << p_searched->value << endl;
  else
    cout << "\n Error: Person you looking for does not exist!" << endl;
}

node* updatePersonEmail(node* p_tree) {
  string searched_key, new_value;
  cout << "\n Enter a first name of person you looking for: ";
  cin >> searched_key;
  cout << "\n Enter a new email: ";
  cin >> new_value;
  return update(p_tree, searched_key, new_value);
}

node* deletePerson(node* p_tree) {
  string searched_key;
  cout << "\n Enter a first name of person you want to remove: ";
  cin >> searched_key;
  return remove(p_tree, searched_key);
}

void displayTree(node* p_tree) {
  if (p_tree != NULL) {
    displayTree(p_tree->p_left);
    cout << p_tree->key << " " << p_tree->value << endl;
    displayTree(p_tree->p_right);
  }
}

node* update(node* p_tree, string searched_key, string new_value) {
  if (p_tree == NULL) {
    return NULL;
  }

  node* p_find_node = search(p_tree, searched_key);
  if (p_find_node != nullptr)
    p_find_node->value = new_value;
  return p_tree;
}

node* insert(node* p_tree, string key, string value) {
  if (p_tree == NULL) {
    node* p_new_tree = new node;
    p_new_tree->p_left = NULL;
    p_new_tree->p_right = NULL;
    p_new_tree->key = key;
    p_new_tree->value = value;
    return p_new_tree;
  }

  if (key < p_tree->key) {
    p_tree->p_left = insert(p_tree->p_left, key, value);
  } else {
    p_tree->p_right = insert(p_tree->p_right, key, value);
  }
  return p_tree;
}

node* search(node* p_tree, string key) {
  if (p_tree == NULL) {
    return NULL;
  }

  else if (key == p_tree->key) {
    return p_tree;
  }

  else if (key < p_tree->key) {
    return search(p_tree->p_left, key);
  } else {
    return search(p_tree->p_right, key);
  }
}

void destroyTree(node* p_tree) {
  if (p_tree != NULL) {
    destroyTree(p_tree->p_left);
    destroyTree(p_tree->p_right);
    delete p_tree;
  }
}

node* remove(node* p_tree, string key) {
  if (p_tree == NULL) {
    return NULL;
  }
  if (p_tree->key == key) {
    if (p_tree->p_left == NULL) {
      node* p_right_subtree = p_tree->p_right;
      delete p_tree;
      return p_right_subtree;
    }
    if (p_tree->p_right == NULL) {
      node* p_left_subtree = p_tree->p_left;
      delete p_tree;
      return p_left_subtree;
    }

    node* p_max_node = find_max(p_tree->p_left);
    p_max_node->p_left = remove_max_node(p_tree->p_left, p_max_node);
    p_max_node->p_right = p_tree->p_right;
    delete p_tree;
    return p_max_node;
  } else if (key < p_tree->key) {
    p_tree->p_left = remove(p_tree->p_left, key);
  } else {
    p_tree->p_right = remove(p_tree->p_right, key);
  }
  return p_tree;
}

node* find_max(node* p_tree) {
  if (p_tree == NULL) {
    return NULL;
  }
  if (p_tree->p_right == NULL) {
    return p_tree;
  }
  return find_max(p_tree->p_right);
}

node* remove_max_node(node* p_tree, node* p_max_node) {
  if (p_tree == NULL) {
    return NULL;
  }
  if (p_tree == p_max_node) {
    return p_max_node->p_left;
  }
  p_tree->p_right = remove_max_node(p_tree->p_right, p_max_node);
  return p_tree;
}