/* 
    Practise Problem
        2. Write a program that counts the number of nodes in a binary tree.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Structures
struct node{
	int key_value;
	node *p_left;
	node *p_right;
};

//Function headers
int countNodes(node *p_tree);

void displayTree(node *p_tree);
node* insert(node *p_tree, int key);
void destroyTree(node *p_tree);
int randRange(int low, int high);
void srandSeed();

//The program starts here
int main(){
    srandSeed();
    node *p_root =  NULL;
    
    for(int i=0; i < 5; i++){
        p_root = insert(p_root, randRange(1,10));
    }

    cout<<"\nBinary Tree \n";
    displayTree(p_root);
    cout<<"\nNumber of nodes in tree: "<<countNodes(p_root)<<endl;

    destroyTree(p_root);
    p_root = NULL;
}

int countNodes(node *p_tree){
    int counter = 1;
    if (p_tree == NULL) return 0;
    else{
        counter += countNodes(p_tree -> p_left);
        counter += countNodes(p_tree -> p_right);
        return counter;
    }
}

void displayTree(node *p_tree){
    if(p_tree != NULL){
        displayTree(p_tree -> p_left);
        cout<<p_tree -> key_value<<endl;
        displayTree(p_tree -> p_right);
    }
}

node* insert(node *p_tree, int key){
	if ( p_tree == NULL ){
		node* p_new_tree = new node;
		p_new_tree->p_left = NULL;
		p_new_tree->p_right = NULL;
		p_new_tree->key_value = key;
		return p_new_tree;
	}

	if( key < p_tree->key_value ){
		p_tree->p_left = insert( p_tree->p_left, key );
	}
	else{
		p_tree->p_right = insert( p_tree->p_right, key );
	}
	return p_tree;
}

void destroyTree(node *p_tree){
	if ( p_tree != NULL ){
		destroyTree( p_tree->p_left );
		destroyTree( p_tree->p_right );
		delete p_tree;
 	}
}

int randRange(int low, int high){
	return rand() % ( high - low + 1) + low; 
}

void srandSeed(){
    int seed = time( NULL );
	cout <<"srandSeed: "<< seed <<" \n";
    srand( seed );
}