/* 
    Practise Problem
        5. Write a program that deletes all of the nodes in a binary tree without using recursion.
*/

#include <iostream>
#include <ctime>

using namespace std;

//Structures
struct node
{
    int key_value;
    node *p_left;
    node *p_right;
};

//Function headers
void displayTree(node *p_tree);
node *insert(node *p_tree, int key);
void destroyTree(node *p_tree);
int randRange(int low, int high);
void srandSeed();

//The program starts here
int main()
{
    srandSeed();
    node *p_root = NULL;

    for (int i = 0; i < 5; i++)
    {
        p_root = insert(p_root, randRange(1, 10));
    }

    cout << "\nBinary Tree \n";
    displayTree(p_root);

    destroyTree(p_root);
    p_root = NULL;
}

void destroyTree(node *p_tree)
{
    node *p, *left, *right;
    node *upper = nullptr;

    for (p = p_tree; p;)
    {
        left = p->p_left;
        right = p->p_right;
        if (left && left != upper)
        {
            p->p_left = upper;
            upper = p;
            p = left;
        }
        else if (right && right != upper)
        {
            p->p_right = upper;
            upper = p;
            p = right;
        }
        else
        {
            delete p;
            p = upper;
            if (p)
                upper = (p->p_left) ? p->p_left : p->p_right;
        }
    }
}

node *insert(node *p_tree, int key)
{
    if (p_tree == NULL)
    {
        node *p_new_tree = new node;
        p_new_tree->p_left = NULL;
        p_new_tree->p_right = NULL;
        p_new_tree->key_value = key;
        return p_new_tree;
    }

    if (key < p_tree->key_value)
    {
        p_tree->p_left = insert(p_tree->p_left, key);
    }
    else
    {
        p_tree->p_right = insert(p_tree->p_right, key);
    }
    return p_tree;
}

void displayTree(node *p_tree)
{
    if (p_tree != NULL)
    {
        displayTree(p_tree->p_left);
        cout << p_tree->key_value << endl;
        displayTree(p_tree->p_right);
    }
}

int randRange(int low, int high)
{
    return rand() % (high - low + 1) + low;
}

void srandSeed()
{
    int seed = time(NULL);
    cout << "srandSeed: " << seed << " \n";
    srand(seed);
}