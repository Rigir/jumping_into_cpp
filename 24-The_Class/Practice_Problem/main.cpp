/* 
    Practise Problem
        1. Take the structure from the practice problem at the end of the last chapter (representing a tic
           tac toe board) and reimplement it using a class, marking the publicly useful methods as public
           and marking the data and any helper methods as private. How much of your code did you have
           to change?
*/

#include "tic-tac-toe.h"

//The program starts here
int main(){
    char player;
    TicTacToe game;

    game.clearBoard();
    while(!game.winner('O') && !game.winner('X') && !game.draw()){
        player = ( player == 'X' ) ? 'O' : 'X';
        game.displayBoard();
        game.playersMove(player);
    }
}

