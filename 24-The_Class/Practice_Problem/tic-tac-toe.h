#include <iostream>

using namespace std;

class TicTacToe{
    public:
        void playersMove(char player);
        void displayBoard();
        void clearBoard();
        bool draw();
        bool winner(char player);
    
    private:
        char BoardSquers[9];
        bool checkWinner(char player);
};