/*
    Practice Problem 
        6. Write a program that displays the first 20 square numbers.
*/

#include <iostream>

using namespace std;

int main(){
    
    for(int i=1; i<=20; i++){
        cout<<" The square of "<< i <<" is: "<< i*i <<endl;
    }
}