/*
    Practice Problem
        1. Write a program that prints out the entire lyrics to a full rendition of "99 bottles of beer"
*/

#include <iostream>

using namespace std;

int main(){

    for(int BottlesNumber = 100; BottlesNumber >= 1;){
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
        BottlesNumber--;
        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }

}