/*
    Practice Problem
        7. Write a program that provides the option of tallying up the results of a poll with 3 possible
           values. The first input to the program is the poll question; the next three inputs are the possible
           answers. The first answer is indicated by 1, the second by 2, the third by 3. The answers are
           tallied until a 0 is entered. The program should then sThe square of ten is one hundhow the results 
	       of the poll—try making a bar graph that shows the results properly scaled to fit on your screen 
	       no matter how many results were entered.
*/

#include <iostream>
#include <string>
#include <cmath> //From here I only take round() function.

using namespace std;

int main(){
    string Question, answer_A, answer_B, answer_C;
    int poll_choice, count_a=0, count_b=0, count_c=0;
    float display_a=0, display_b=0, display_c=0, sum_answers=0;

    cout<<"This program is simple poll! I hope you will like it! \n";

    do{
    cout<<"Please, enter your Question: \n";
        getline(cin, Question);
    }while(Question == "");

        do{
        cout<<"Enter your first answer: \n";
            getline(cin, answer_A);
        }while(answer_A == "");

        do{
        cout<<"Enter your secound answer: \n";
            getline(cin, answer_B);
        }while(answer_B == "");
        
        do{
        cout<<"Enter your last anwser: \n";
            getline(cin, answer_C);
        }while(answer_C == "");

    do{
        cout<<"\n  \t\t ~| Poll |~ \n";
        cout<<Question<<endl;
        cout<<" 1."<<answer_A<<endl;
        cout<<" 2."<<answer_B<<endl;
        cout<<" 3."<<answer_C<<endl;
        cout<<"\n  If you want to count votes please enter '0' \n\n";
        
        cout<<"  Please, enter your choise: ";
            cin>>poll_choice;

            if(poll_choice == 1){
                count_a++;
            }
            else if(poll_choice == 2){
                count_b++;
            }
            else if(poll_choice == 3){
                count_c++;
            }
            else if(!(poll_choice >= 0 && poll_choice <= 3) ){
                cout<<" ~| The choice must exist, try again! |~ \n";
            }
    }while(poll_choice != 0);

    //Sum up all the answers
    sum_answers = count_a + count_b + count_c;

    //Assigns variable values to display
    display_a = round((count_a * 100) / sum_answers);
    display_b = round((count_b * 100) / sum_answers);
    display_c = round((count_c * 100) / sum_answers);

    //Display result
    cout<<"\n  \t\t ~| Result |~ \n";
    cout<<Question<<"\n\n";

    //Display Answer A
    cout<<" 1."<<answer_A<<endl;
        cout<<"\t"<<count_a<<" Votes \t";
            if(count_a == 0)
                cout << " 0%";
            else if(display_a<10)
                cout<<" "<<display_a<<"%";
            else
                cout<<display_a<<"%";

        cout<<" [ ";

            for(int i=0; i < round(display_a/2); i++)
                cout<<"*";
            
        cout<<" ]\n\n";
    
    //Display Answer B
    cout<<" 2."<<answer_B<<endl;
        cout<<"\t"<<count_b<<" Votes \t";
            if(count_b == 0)
                cout << " 0%";
            else if(display_b<10)
                cout<<" "<<display_b<<"%";
            else
                cout<<display_b<<"%";

        cout<<" [ ";

            for(int i=0; i < round(display_b/2); i++)
                cout<<"*";

        cout<<" ]\n\n";

    //Display Answer C
    cout<<" 3."<<answer_C<<endl;
        cout<<"\t"<<count_c<<" Votes \t";
            if(count_c == 0)
                cout << " 0%";
            else if(display_c<10)
                cout<<" "<<display_c<<"%";
            else
                cout<<display_c<<"%";

        cout<<" [ ";

            for(int i=0; i < round(display_c/2); i++)
                cout<<"*";
        
        cout<<" ]\n";
}
