/*
    Practice Problem
        3. Write a program that computes a running sum of inputs from the user, terminating 
        when the user gives an input value of 0.
*/
#include <iostream>

using namespace std;

int main(){
    int number, sum=0;

    cout<<"\n This program will sum all your number's ";
    cout<<"\n If you want to finish adding, enter '0' \n\n";
    do{
        cout<<" Please enter a number: ";
            cin>>number;
        sum+=number;
    }while(number != 0);

    cout<<"\n Result: "<<sum<<endl;
}