/*
    Practice Problem
        2. Write a menu program that lets the user select from a list of options, and 
           if the input is not one of the options, reprint the list.
*/

#include <iostream>

using namespace std;

int main(){
    int menu_choice;
    
    while(!(menu_choice >= 1 && menu_choice <= 3)){
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Options \n";
        cout<<" 3. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
    }

    if (menu_choice == 1){
        cout<<" Let's Rock! \n";
    }
    else if ( menu_choice == 2){
        cout<<" Looking for a resolution ? \n";
    }
    else if(menu_choice == 3){
        cout<<" It's now safe to turn off your computer! \n";
    }
}