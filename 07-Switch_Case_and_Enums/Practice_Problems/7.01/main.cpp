/* 
    Practise Problem
        1. Rewrite the menu program you wrote in the Practice Problems for Functions using switch-case.
*/

#include <iostream>

using namespace std;

//Function headers
void startGame();
void calculator();
void songBottles();
void options();
void exit();

//The program starts here
int main(){
    int menu_choice;
    
    while(!(menu_choice >= 1 && menu_choice <= 6)){
        cout<<"  ~| Menu |~ \n";
        cout<<" 1. Start Game \n";
        cout<<" 2. Calculator \n";
        cout<<" 3. Song: '99 bottles of bear' \n";
        cout<<" 4. Options \n";
        cout<<" 5. Exit \n";

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;
    }

    switch(menu_choice){
        case 1:
            startGame();
            break;
        case 2:
            calculator();
            break;
        case 3:
            songBottles();
            break;
        case 4:
            options();
            break;
        case 5:
            exit();
            break;
        default:
            cout << "There is no more options, you can go now! \n";
		    break;
    }
}

void startGame(){
    cout<<" Let's Rock! \n";
}

void options(){
    cout<<" Looking for a resolution ? \n";
}

void exit(){
    cout<<" It's now safe to turn off your computer! \n";
}

void songBottles(){
    for(int BottlesNumber = 100; BottlesNumber >= 1;){
        cout<< BottlesNumber <<" bottles of beer on the wall, "<< BottlesNumber <<" bottles of beer! \n";
        BottlesNumber--;

        if(BottlesNumber > 0){
            cout<<"Take one down and pass it around, "<< BottlesNumber <<" bottles of beer on the wall \n"<<endl;
        }
        else{
            cout<<"Take one down and pass it around, no more bottles of beer on the wall. \n";
            cout<<"\nNo more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall... \n";
        }
    }
}

void calculator(){
     char arithmetic_operations;
     int number_one, number_two;

    cout<<"\n\t ~| Calculator |~  \n";
    cout<<"(Please enter the arithmatic operator): ";
        cin>>arithmetic_operations;

    cout<<"\n Enter first number: ";
        cin>>number_one;
    cout<<" Enter secound number: ";
        cin>>number_two;

    switch (arithmetic_operations){
        case '+':
            cout<<"\n Result: "<< number_one + number_two <<endl;
            break;
        case '-':
            cout<<"\n Result: "<< number_one + number_two <<endl;
            break;
        case '/': case '%':
            cout<<"\n Result: "<< number_one + number_two <<endl;
            break;
        case '*': case 'x':
            cout<<"\n Result: "<< number_one + number_two <<endl;
            break;
        default:
            cout<<"\n I did not find such an arithmetic operation "<<endl;
            break;
    }
}