/*
    Practice Problem
        3. Write a two-player tic-tac-toe game; use enums when possible to represent the values of the board
*/

#include <iostream>

using namespace std;

//Global variables
enum databaseAction { read, write, clear };
char player,P1,P2,P3,P4,P5,P6,P7,P8,P9; 

//Function headers
void playersMove();
void displayBoard();
bool draw();
bool winner(char player);
char database(int move, int action);
char databaseOption(char square, int action);

//The program starts here
int main(){
    for(int i=1; i<=9; i++) database(i,clear);
    while(!winner('O') && !winner('X') && !draw()){
        displayBoard();
        playersMove();
    }
}

void playersMove(){
    int move;

    player = ( player == 'X' ) ? 'O' : 'X' ;
        
        while(!(move >= 1 && move <= 9 && database(move, read) == ' ')){
            cout<<"\nPlayer "<< player <<" Your turn: ";
                cin>>move;
            
            //error messages
            if(!(move >= 1 && move <= 9))
                cout<<"\n This square doesn't exist! Try again! \n";
            else if(!(database(move, read) == ' '))
                cout<<"\n Your opponent choose this option before! Try again! \n";
        }  
        database(move, write);
}   

bool winner(char player){
    bool win = false;

    //Columns
    for(int i=1; i<=3; i++)
        if(( database(i,read) == player) && (database(i+3,read) == player) && (database(i+6,read) == player)) win = true;

    //Rows
    for(int i=1; i<=7; i+= 3)
        if(( database(i,read) == player) && (database(i+1,read) == player) && (database(i+2,read) == player)) win = true;

    //Diagonals
    if(( database(1,read) == player) && (database(5,read) == player) && (database(9,read) == player)) win = true;
    if(( database(3,read) == player) && (database(5,read) == player) && (database(7,read) == player)) win = true;


    if(win){
        displayBoard();
        cout<<"\n Player: "<< player << " wins! \n\n";
        return true;
    }
    else{
        return false;
    }
        
}

bool draw(){
    for(int i=1; i<=9; i++)
        if(database(i,read) == ' ') return false;
    displayBoard();
    cout<<"\n    DRAW   \n\n";
    return true;
}

void displayBoard(){
    cout<<"\n";
    for(int i=1; i <= 9; i++){
        cout<<" "<< database(i,read)<< " ";
        if(i % 3)
            cout<<"|";
        else if(i != 9)
            cout<<"\n---+---+---\n";
        else cout<<endl;
    }
}

char database(int move, int action){
    switch(move){
        case 1:
            return P1 = databaseOption(P1,action);
        case 2:
            return P2 = databaseOption(P2,action);
        case 3:
            return P3 = databaseOption(P3,action);
        case 4:
            return P4 = databaseOption(P4,action);
        case 5:
            return P5 = databaseOption(P5,action);
        case 6:
            return P6 = databaseOption(P6,action);
        case 7:
            return P7 = databaseOption(P7,action);
        case 8:
            return P8 = databaseOption(P8,action);
        case 9:
            return P9 = databaseOption(P9,action);
        default:
            return '!';
    }
}

char databaseOption(char square, int action){
    switch(action){
        case read: return square;
        case write: return square = player;
        case clear: return square = ' ';
        default: return '!';
    }
}
