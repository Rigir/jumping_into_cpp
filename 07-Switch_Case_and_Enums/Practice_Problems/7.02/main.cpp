/* 
    Practise Problem
        2. Write a program that outputs the results of the 12 days of Christmas using switch-case (hint:
           you might want to take advantage of fall-through cases)
*/

#include <iostream>
#include <string>

using namespace std;

//Function headers
string whichDay(int i);
void whatSend(int i);

//The program starts here
int main(){
    for(int i=1; i<=12; i++){
        cout<<"\n On the "<< whichDay(i) <<" day of Christmas my true love sent to me \n";
        whatSend(i);
    }
}

void whatSend(int i){
    switch(i){
        case 12:
            cout<<" Twelve drummers drumming \n";
        case 11:
            cout<<" Eleven pipers piping \n";
        case 10:
            cout<<" Ten lords a-leaping \n";
        case 9:
            cout<<" Nine ladies dancing \n";
        case 8:
            cout<<" Eight maids a-milking \n";
        case 7:
            cout<<" Seven swans a-swimming \n";
        case 6:
            cout<<" Six geese a-laying \n";
        case 5:
            cout<<" Five golden rings \n";
        case 4:
            cout<<" Four calling birds \n";
        case 3:
            cout<<" Three french hens \n";
        case 2:
            cout<<" Two turtle doves, and \n";
        case 1:
            cout<<" A partridge in a pear tree \n";
    }
}

string whichDay(int i){
    switch(i){
        case 1:
            return "first";
        case 2: 
            return "second";
        case 3: 
            return "third";
        case 4: 
            return "fourth";
        case 5: 
            return "fifth";
        case 6: 
            return "sixth";
        case 7: 
            return "seventh";
        case 8: 
            return "eighth";
        case 9: 
            return "ninth";
        case 10: 
            return "tenth";
        case 11: 
            return "eleventh";
        case 12: 
            return "twelfth";
    }
}