/*
    Practice Problem
        2. Write a program that takes in 50 values and prints out the highest, the lowest, the average and
           then all 50 input values, one per line.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
void srandSeed();
void displayArray(int array[], int size);

//The program starts here!
int main (){
    int array[ 50 ];
    int max=0,min=100;
    float avg=0;
	srandSeed();
    
	for ( int i = 0; i < 50; i++ ){
		array[ i ] = rand() % 100;

        //Assigning min and max values
        if( array[i] > max){
            max = array[i];
        }
        else if( array[i] < min){
            min = array[i];
        }

        avg += array[i];
	}
    avg /= 50;
    cout<<"\n Min: "<< min <<" Max: "<< max <<" Avg: "<< avg <<"\n";
    displayArray(array,50);
}

void displayArray (int array[], int size){
	cout << "{";
	for ( int i = 0; i < size; i++ ){
		if ( i != 0 ) cout << ", ";
		cout << array[ i ];
	}
	cout << "} \n";
}

void srandSeed(){
    int seed = time(NULL);
    cout<<" srandSeed: "<<seed<<endl;
    srand(seed);
}