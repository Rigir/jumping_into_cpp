/*
    Practice Problem
        5. Make your tic-tac-toe game into a version of connect-4 that allow boards bigger than 3 by 3 but
           requires 4-in-a-row to win. Allow the players to specify the size of the board while the program
           is running. 
           (Hint: right now, you have to define your board to be a fixed size at compile time, so
           you may need to limit the maximum size of the board.)
*/

#include <iostream>

using namespace std;

//Global variables
char BoardSquers[10][10];
char player;
int columns,rows;

//Function headers
void displayBoard();
void sizeOfBoard();
void clearBoard();
void playersMove();
bool draw();
bool winner(char player);
bool checkWinner(char player);


//The program starts here
int main(){
    sizeOfBoard();
    clearBoard();
    while(!winner('O') && !winner('X') && !draw()){
        displayBoard();
        playersMove();
    }
    
}

void playersMove(){
    int rowMove, columnMove;

    player = ( player == 'X' ) ? 'O' : 'X' ;

    while(!((rowMove >= 1 && rowMove <= rows) && (columnMove >= 1 && columnMove <= columns) && BoardSquers[rowMove-1][columnMove-1] == ' ')){
        cout<<"\nPlayer "<< player <<" Your turn Col|Row : ";
            cin>>columnMove>>rowMove;
        
        //error messages
        if(!(rowMove >= 1 && rowMove <= rows) || !(columnMove >= 1 && columnMove <= columns) )
            cout<<"\n Error: This square doesn't exist! Try again! \n";
        else if(BoardSquers[rowMove-1][columnMove-1] != ' ')
            cout<<"\n Error: Your opponent choose this option before! Try again! \n";
    }  
    BoardSquers[rowMove-1][columnMove-1] = player;
}

bool winner(char player){
    if(checkWinner(player)){
        displayBoard();
        cout<<"\n Player: "<< player << " wins! \n\n";
        return true;
    }

    return false;  
}

bool checkWinner(char player){
    bool win = false;

    //Columns
    for(int i=0; i<rows; i++)
        for(int j=0; j<columns-3; j++)
            if(( BoardSquers[i][j] == player) && (BoardSquers[i][j+1] == player) && (BoardSquers[i][j+2] == player) && (BoardSquers[i][j+3] == player)) 
                win = true;

    //Rows
    for(int i=0; i<rows-3; i++)
        for(int j=0; j<columns; j++)
            if(( BoardSquers[i][j] == player) && (BoardSquers[i+1][j] == player) && (BoardSquers[i+2][j] == player) && (BoardSquers[i+3][j] == player)) 
                win = true;

    //Diagonals
    for(int i=0; i<rows-3; i++) //from left to right
        for(int j=0; j<columns-3; j++)
            if(( BoardSquers[i][j] == player) && (BoardSquers[i+1][j+1] == player) && (BoardSquers[i+2][j+2] == player) && (BoardSquers[i+3][j+3] == player)) 
                win = true;
    
    for(int i=0; i<rows-3; i++) //from right to left
        for(int j=0; j<columns-3; j++)
            if(( BoardSquers[i][j+3] == player) && (BoardSquers[i+1][j+2] == player) && (BoardSquers[i+2][j+1] == player) && (BoardSquers[i+3][j] == player)) 
                win = true;

    return win;
}

bool draw(){
    for(int i=0; i<rows; i++)
        for(int j=0; j<columns; j++)
            if(BoardSquers[i][j] == ' ') return false;
    displayBoard();
    cout<<"\n    DRAW   \n\n";
    return true;
}

void sizeOfBoard(){
    cout<<"Enter the size of the game board.\nColumns and Rows need to be in range from 4 to 9. \n";
    
    while(!(columns >= 4 && columns <= 9) || !(rows >= 4 && rows <= 9)){
        cout<<"\nEnter, the column size: ";
            cin>>columns;
        cout<<"Enter, the row size: ";
            cin>>rows;

        //error messages
        if(!(columns >= 4 && columns <= 9) && !(rows >= 4 && rows <= 9))
            cout<<" Error: You enter wrong size of row and column \n";
        else if (!(columns >= 4 && columns <= 9))
            cout<<" Error: You enter wrong size of column \n";
        else if (!(rows >= 4 && rows <= 9))
            cout<<" Error: You enter wrong size of row \n";
    }
}

void clearBoard(){
    for(int i=0; i<rows; i++)
        for(int j=0; j<columns; j++)
            BoardSquers[i][j] = ' ';
}

void displayBoard(){
    for(int i=1; i<= rows; i++){
        cout<<"\n ";
        for(int j=1; j<= columns; j++){
                if(i!= 1){
                    cout<<"---";
                    if(j != columns) cout<<"+";  
                }
                else cout<<" "<<j<<"  ";  
        }

        cout<<"\n"<<i;
        for(int j=1; j<= columns; j++){
            cout<<" "<<BoardSquers[i-1][j-1]<<" ";
            if(j != columns) cout<<"|";
        }
    }
    cout<<"\n";
}

