/*
    Practice Problem
        6. Make a two-player checkers program that allows each player to make a move, and checks for
           legal moves and whether the game is over. Be sure to support kinging! Feel free to add support
           for any house rules that you use when you play. Consider making the kind of rules used an
           option at program startup.
*/

#include <iostream>
#include <string>

using namespace std;

//Global variables
string BoardSquers[10][10];

//Function headers

//Board
void clearBoard(); 
void fillBoard(); 
void displayBoard();
void rotateBoard(string player);
void copyBoard(string array[][10],string player);
void displayPawns(int num1, int num2);

//Support
bool inRange(int num1, int num2);
bool isDivisible(int num1, int num2);
int findMiddleValue(int number, int pickedValue);

//About Player
bool anotherTurn();
bool winner(string player);
void playerMoves(string player);
void crowning(string player, int rowMove, int columnMove);

//Path
bool analyzePath(int rowMove, int columnMove, int rowPicked, int columnPicked);
bool checkPathDirection(int rowMove, int columnMove, int rowPicked, int columnPicked);
bool checkPath(string path[], int size);
bool isPathEmpty(string path[], int size);
bool isYourPawn(string path[], int size);
bool twoPawnsInARow(string path[], int size);
bool canMenCapture(string path[], int size);

//The program starts here
int main(){
    string player;
    clearBoard();
    fillBoard();
    do{
        player = ( player[0] == 'W' ) ? "Black" : "White";
        playerMoves(player);
        rotateBoard(player);
    }while(!winner(player));
}

void playerMoves(string player){
    int rowMove, columnMove, rowPicked, columnPicked;

    do{
        displayBoard();
        do{
            do{
                cout<<" "<< player <<"'s turn Col|Row : ";
                    cin>>columnMove>>rowMove;
            }while(!inRange(columnMove, rowMove));
            
            //error messages
            if(BoardSquers[rowMove][columnMove][0] != player[0]) 
                cout<<"\n Error: You must choose your pawn! \n";
        }while(BoardSquers[rowMove][columnMove][0] != player[0]);

        do{
            do{
                cout<<" to: ";
                    cin>>columnPicked>>rowPicked;
            }while(!inRange(columnPicked, rowPicked));

            //error messages
            if(isDivisible(columnPicked, rowPicked)) 
                cout<<"\n Error: Use only the dark square's of the checkeredboard. \n";
            if(BoardSquers[rowPicked][columnPicked] != " ") 
                cout<<"\n Error: There is an opponent's or your's pawn in this square. \n";
        }while(isDivisible(rowPicked,columnPicked) || BoardSquers[rowPicked][columnPicked] != " ");
    }while(analyzePath(rowMove, columnMove, rowPicked, columnPicked));
}

bool analyzePath(int rowMove, int columnMove, int rowPicked, int columnPicked){
    int row=rowMove, column=columnMove, size=0;
    string path[10];

    if(!checkPathDirection(rowMove, columnMove, rowPicked, columnPicked)) return true;

    //Get Path
    for(int i=0; i<10; i++){
        path[i] = BoardSquers[rowMove][columnMove];
        if(rowMove == rowPicked && columnMove == columnPicked){
            size = ++i;
            break;
        }
        rowMove = findMiddleValue(rowMove, rowPicked);
        columnMove = findMiddleValue(columnMove, columnPicked);
    }
    
    if(!checkPath(path, size)) return true;

    //Execute path
    BoardSquers[rowPicked][columnPicked] = path[0];
    for(int i=0; i<size-1; i++){
        BoardSquers[row][column] = " ";
        row = findMiddleValue(row, rowPicked);
        column = findMiddleValue(column, columnPicked);
    }
    return false;         
}


bool checkPath(string path[], int size){
    if(isYourPawn(path, size)) return false;
    if(path[0][1] == 'M'){
        if( size == 2  && path[1] == " ") return true;
        if(!canMenCapture(path, size)) return false;
    }
    else{
        if(isPathEmpty(path, size)) return true;
        if(twoPawnsInARow(path, size)) return false;
    }
    return true;
}

bool isPathEmpty(string path[], int size){
    for(int i=1; i<size; i++)
        if( path[i] != " ") return false;
    return true;
}

bool isYourPawn(string path[], int size){
    for(int i=1; i<size; i++)
        if( path[0][0] == path[i][0]){
             cout<<"\n Error: You can't jump over your own pawn!.\n";
             return true;
        } 
    return false;
}

bool twoPawnsInARow(string path[], int size){
    char powns[2] = {'M', 'K'};
    for(int i=1; i<size-1; i++)
        for(int j=0; j<2; j++)
            for(int k=0; k<2; k++)
                if( path[i][1] == powns[j] &&  path[++i][1] == powns[k]){
                    cout<<"\n Error: You can't jump over two pawns!.\n";
                    return true;
                }
    return false;
}

bool canMenCapture(string path[], int size){
    for(int i=1; i<size-1; i+=2)
        if( (path[i] == " " || path[i] != " ") && path[++i] != " "){
            cout<<"\n Error: You cant move this pawn that far!.\n";
            return false;
        }
    return true;
}

bool checkPathDirection(int rowMove, int columnMove, int rowPicked, int columnPicked){
    for(int i=-9; i<=9; i++){
        int position[2][2] = {
            {(rowMove+i),(rowMove-i)}, 
            {(columnMove+i),(columnMove-i)}
        };
        for(int j=0; j<2; j++)
            for(int k=0; k<2; k++)
                if(position[0][j] == rowPicked && position[1][k] == columnPicked) return true; 
    }
    cout<<"\n Error: You can't move a pawn in this direction!. \n";
    return false;
}

void fillBoard(){
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++)
            if(!isDivisible(i,j)){
                if(i < 4) BoardSquers[i][j] = "BM";
                if(i > 5) BoardSquers[i][j] = "WM";
            }
}

int findMiddleValue(int number, int pickedValue){
    if(number == pickedValue) return number;
    else if(number > pickedValue) return --number;
    return ++number;
}

bool inRange(int num1, int num2){
    if((num1 >= 0 && num1 <= 9) && (num2 >= 0 && num2 <= 9)) return true;
    return false;
}

bool isDivisible(int num1, int num2){
	return (num1+num2) % 2 == 0;
}

bool winner(string player){
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++)
            if(BoardSquers[i][j][0] != player[0] && BoardSquers[i][j] != " ") return false;
    displayBoard();
    cout<<"\n The "<< player << " player wins! \n\n";
    return true;
}

void clearBoard(){
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++)
            BoardSquers[i][j] = " ";
}

void rotateBoard(string player){
    string tempBoard[10][10];
    copyBoard(tempBoard, player);
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++)
            BoardSquers[i][j] = tempBoard[9-i][9-j];
}

void copyBoard(string array[][10], string player){
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++){
            crowning(player, i, j);
            array[i][j] = BoardSquers[i][j];
        }
}   

void crowning(string player, int rowMove, int columnMove){
    string square =  BoardSquers[rowMove][columnMove];
    if(rowMove == 0 && !isDivisible(rowMove,columnMove)){
        if(square[0] == player[0] && square[1] == 'M'){
            BoardSquers[rowMove][columnMove][1] = 'K';
        }
    }
}

void displayBoard(){
    cout<<"\n\t International Draughts \n";
    for(int i=0; i<=9; i++){
        cout<<"\n ";
        for(int j=0; j<=9; j++){
                if(i!= 0){
                    cout<<"---";
                    if(j != 9) cout<<"+";  
                }
                else cout<<" "<<j<<"  ";  
        }
        cout<<"\n"<<i;
        for(int j=0; j<=9; j++){
            displayPawns(i,j);
            if(j != 9) cout<<"|";
        }
    }
    cout<<"\n\n";
}

void displayPawns(int num1, int num2){
    if(BoardSquers[num1][num2] == "WM") cout<<" \u26C0 ";
    if(BoardSquers[num1][num2] == "WK") cout<<" \u26C1 ";
    if(BoardSquers[num1][num2] == "BM") cout<<" \u26C2 ";
    if(BoardSquers[num1][num2] == "BK") cout<<" \u26C3 ";
    if(BoardSquers[num1][num2] == " ")  cout<<"   ";
    //cout<<" "<<BoardSquers[num1][num2]<<" ";
}