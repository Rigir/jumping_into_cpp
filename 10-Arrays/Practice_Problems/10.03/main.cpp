/*
    Practice Problem
        3. Write a program that detects if a list is sorted or not, and if it is not sorted, sort it.
*/

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

//Function Heders
int findSmallestRemainingElement (int array[], int size, int index);
bool chceckSorted(int arrey[], int size); 
void displayArray (int array[], int size);
void swap (int array[], int first_index, int second_index);
void sort (int array[], int size);

//The program starts here!
int main (){
	srand( time( NULL ) ); 

    int array[ 5 ];

	for ( int i = 0; i < 5; i++ ){
		array[ i ] = rand() % 20;
	}

	cout << "Original array: ";
	displayArray( array, 5 );
	cout << '\n';
	
	if(chceckSorted(array,5))
		cout<<"Is sorted \n";
	else{
		cout<<"Isn't sorted \n";
		sort( array, 5 );
		cout << "Sorted array: ";
		displayArray( array, 5 );
		cout << '\n';
	}	
}

// Function that returns true if array is
// sorted in non-decreasing order
bool chceckSorted(int arrey[], int size){
	// if array has one or no element
	if(size == 0 || size == 1)
		return true;
	
	// Search for unsorted pair
	for(int i = 0; i < size; i++ ){
		if(arrey[i]>arrey[i+1])
			return false;
	}
	
	// If arrey is sorted
	return true;
}

void sort (int array[], int size){
	for ( int i = 0; i < size; i++ ){
		int index = findSmallestRemainingElement( array, size, i );
		swap( array, i, index );
	}
}

int findSmallestRemainingElement (int array[], int size, int index){
	int index_of_smallest_value = index;

	for (int i = index + 1; i < size; i++){
		if ( array[ i ] < array[ index_of_smallest_value ] ){
			index_of_smallest_value = i;
		}
	}
	return index_of_smallest_value;
}

void swap (int array[], int first_index, int second_index){
	int temp = array[ first_index ];

	array[ first_index ] = array[ second_index ];
	array[ second_index ] = temp;
}

void displayArray (int array[], int size){
	cout << "{";
	for ( int i = 0; i < size; i++ ){
		if ( i != 0 ){
			cout << ", ";
		}
		cout << array[ i ];
	}
	cout << "}";
}
