/*
    Practice Problem
        4. Write a small tic-tac-toe program that allows two players to play tic-tac-toe competitively. Your
           program should check to see if either player has won, or if the board is filled completely (with
           the game ending in a tie). 
           Bonus: can you make your program detect if the game cannot be won
           by either side before the entire grid is filled?
*/

#include <iostream>

using namespace std;

//Global variables
char BoardSquers[9];

//Function headers
void playersMove(char player);
void displayBoard();
int freeSpaceLeft();
bool draw();
bool winner(char player);
bool checkWinner(char player, char BoardSquers[]);
bool canPlayersWin(char player); //Bonus

//The program starts here
int main(){
    char player;

    for(int i=0; i<9; i++) BoardSquers[i] = ' ';
    while(!winner('O') && !winner('X') && !draw()){
        player = ( player == 'X' ) ? 'O' : 'X';
        if(freeSpaceLeft()<2 && canPlayersWin(player))
            cout<<"\nNobody can win this game!\n";
        displayBoard();
        playersMove(player);
    }
}

bool canPlayersWin(char player){
    char temp[9];

    for(int i=0; i<9; i++)
        temp[i] = ( BoardSquers[i] == ' ') ? player : BoardSquers[i];
        
    if(!checkWinner(player, temp)) return true;
    return false;
}

void playersMove(char player){
    int move;

    while(!(move >= 1 && move <= 9 && BoardSquers[move-1] == ' ')){
        cout<<"\nPlayer "<< player <<" Your turn: ";
            cin>>move;
        
        //error messages
        if(!(move >= 1 && move <= 9))
            cout<<"\n Error: This square doesn't exist! Try again! \n";
        else if(BoardSquers[move-1] != ' ')
            cout<<"\n Error: Your opponent choose this option before! Try again! \n";
    }  
    BoardSquers[move-1] = player;
}  

bool winner(char player){
    if(checkWinner(player, BoardSquers)){
        displayBoard();
        cout<<"\n Player: "<< player << " wins! \n\n";
        return true;
    }
    return false;  
}

bool checkWinner(char player, char BoardSquers[]){
    bool win = false;

    //Columns
    for(int i=0; i<=2; i++)
        if(( BoardSquers[i] == player) && (BoardSquers[i+3] == player) && (BoardSquers[i+6] == player)) win = true;

    //Rows
    for(int i=0; i<=6; i+=3)
        if(( BoardSquers[i] == player) && (BoardSquers[i+1] == player) && (BoardSquers[i+2] == player)) win = true;

    //Diagonals
    if(( BoardSquers[0] == player) && (BoardSquers[4] == player) && (BoardSquers[8] == player)) win = true;
    if(( BoardSquers[2] == player) && (BoardSquers[4] == player) && (BoardSquers[6] == player)) win = true;

    return win;
}

int freeSpaceLeft(){
    int counter = 0;
        for(int i=0; i<9;i++)
            if( BoardSquers[i] == ' ')
                counter++;
    return counter;
}

bool draw(){
    for(int i=0; i<9; i++)
        if(BoardSquers[i] == ' ') return false;
    displayBoard();
    cout<<"\n    DRAW   \n\n";
    return true;
}

void displayBoard(){
    cout<<"\n";
    for(int i=1; i<=9; i++){
        cout<<" "<< BoardSquers[i-1] << " ";
        if(i % 3)
            cout<<"|";
        else if(i != 9)
            cout<<"\n---+---+---\n";
        else cout<<endl;
    }
}