/* 
    Practise Problem
        5. Write a two-player game of "connect 4" where the user can set the width and height of the
           board and each player gets a turn to drop a token into the slot. Display the board using + for one
           side, x for the other, and _ to indicate blank spaces.
*/

#include <iostream>
#include <string>

using namespace std;

//Function headers
bool isNegative(int width, int length);
void fillArray(char **p_p_array, int width, int length);
void getPlayerMoves(char **p_p_array, string player, int width, int length);
int checkPlayerMove(char **p_p_array, int move, int width);
bool winner(char **p_p_array, string player, int width, int length);
bool checkWinner(char **p_p_array, string player, int width, int length);
bool draw(char **p_p_array, int width, int length);
void printArray(char **p_p_array, int width, int length);

//The program starts here
int main(){
    int width, length;
    string player;

    cout<<"\n Please, enter board size. \n";
    do{
        cout<<" Width: ";
            cin>>width;
        cout<<" Length: ";
            cin>>length;
    }while(isNegative(width, length));

    // Create a 2D dynamically allocated array.
    char **p_p_BoardSquers = new char*[width];
    for(int i = 0; i < width; i++)
        p_p_BoardSquers[i] = new char[length];

    // Here you operate on data.
    fillArray(p_p_BoardSquers, width, length);   
    do{
        player = ( player[0] == 'Y' ) ? "Red" : "Yellow";
        printArray(p_p_BoardSquers, width, length);
        getPlayerMoves(p_p_BoardSquers, player, width, length);
    }while(!winner(p_p_BoardSquers, player, width, length) && !draw(p_p_BoardSquers, width, length));

    // Delete a 2D dynamically allocated array.
    for(int i = 0; i < width; i++)
       delete []p_p_BoardSquers[i];
    delete []p_p_BoardSquers;
    p_p_BoardSquers = NULL;
}

void getPlayerMoves(char **p_p_array, string player, int width, int length){
    int move, empty;
    do{
        do{
            cout<<"\nPlayer "<< player <<" Your turn : ";
                cin>>move;

            //error messages
            if(!(move >= 1 && move <= length))
                cout<<"\n Error: This column doesn't exist! Try again! \n";
        }while(!(move >= 1 && move <= length));
        empty = checkPlayerMove(p_p_array, move, width);
    }while(empty == 0);
    p_p_array[empty-1][move-1] = player[0];
}

int checkPlayerMove(char **p_p_array, int move, int width){
    for(int i = width; i >= 1; i--)
        if(p_p_array[i-1][move-1] == '_') return i; 
    cout<<"\n Error: This column is already full ! Try again! \n";
    return 0;
}

bool isNegative(int width, int length){
    if(width < 4 || length < 4){
        cout<<"\n Error: The entered values must be greater then three \n";
        return true;
    }
    return false;
}

void fillArray(char **p_p_array, int width, int length){
    for(int i = 0; i < width; i++)
        for(int j = 0; j < length; j++)
            p_p_array[i][j] = '_';
}

bool draw(char **p_p_array, int width, int length){
    for(int i=0; i<width; i++)
        for(int j=0; j<length; j++)
            if(p_p_array[i][j] == '_') return false;
    printArray(p_p_array, width, length);
    cout<<"\n    DRAW   \n";
    return true;
}

bool winner(char **p_p_array, string player, int width, int length){
    if(checkWinner(p_p_array, player, width, length)){
        printArray(p_p_array, width, length);
        cout<<"\n Player: "<< player << " wins! \n\n";
        return true;
    }
    return false;  
}

bool checkWinner(char **p_p_array, string player, int width, int length){
    bool win = false;

    //Columns
    for(int i=0; i<width; i++)
        for(int j=0; j<length-3; j++)
            if(( p_p_array[i][j] == player[0]) && (p_p_array[i][j+1] == player[0]) && (p_p_array[i][j+2] == player[0]) && (p_p_array[i][j+3] == player[0])) 
                win = true;

    //Rows
    for(int i=0; i<width-3; i++)
        for(int j=0; j<length; j++)
            if(( p_p_array[i][j] == player[0]) && (p_p_array[i+1][j] == player[0]) && (p_p_array[i+2][j] == player[0]) && (p_p_array[i+3][j] == player[0])) 
                win = true;

    //Diagonals
    for(int i=0; i<width-3; i++) //from left to right
        for(int j=0; j<length-3; j++)
            if(( p_p_array[i][j] == player[0]) && (p_p_array[i+1][j+1] == player[0]) && (p_p_array[i+2][j+2] == player[0]) && (p_p_array[i+3][j+3] == player[0])) 
                win = true;
    
    for(int i=0; i<width-3; i++) //from right to left
        for(int j=0; j<length-3; j++)
            if(( p_p_array[i][j+3] == player[0]) && (p_p_array[i+1][j+2] == player[0]) && (p_p_array[i+2][j+1] == player[0]) && (p_p_array[i+3][j] == player[0])) 
                win = true;

    return win;
}

void printArray(char **p_p_array, int width, int length){
    cout<<"\n Connect Four \n";
    for(int i = 0; i <= width+1; i++){
        for(int j = 0; j < length; j++){
            if( j == 0 && i == 0) cout<<" ";
            else if(j == 0) cout<<"+";

            if(i==0) cout<<" "<<j+1<<" ";
            if(i>0 && i<width+1) cout<<" "<<p_p_array[i-1][j]<<" ";
            if(i == width+1) cout<<" x ";

            if( j == length-1 && i == 0) cout<<" ";
            else if(j == length-1) cout<<"+";
        }
        cout<<endl;
    }
}

