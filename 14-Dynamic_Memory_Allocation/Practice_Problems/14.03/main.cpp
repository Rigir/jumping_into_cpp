/* 
    Practise Problem
        3. Write a program that prints out the memory addresses of each element in a 2-dimensional
           array. Check to see if the values printed out make sense to you based on the way I explained it
           before.
*/

#include <iostream>

using namespace std;

//The program starts here
int main(){
    int lenght = 2, width = 3, 
        arr[lenght][width];

    cout<<"\n ===| "<<&arr<<" <-- Where array is declared."<<endl;
    for(int i = 0; i < lenght; i++){
        cout<<" --| "<<&arr[i]<<" <-- lenght[ "<<i<<" ]"<<endl;
        for(int j = 0; j < width; j++)
            cout<<" > "<<&arr[i][j]<<" <-- width[ "<<j<<" ]"<<endl;
    }
}
