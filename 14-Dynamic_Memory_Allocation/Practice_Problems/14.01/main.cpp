/* 
    Practise Problem
        1. Write a function that builds the multiplication table of arbitrary dimensions.
*/

#include <iostream>

using namespace std;

//Function headers
bool isNegative(int width, int length);
void fillArray(int **p_p_array, int width, int length);
void printArray(int **p_p_array, int width, int length);

//The program starts here
int main(){
    int width, length;

    cout<<"\n Multiplication Table \n";
    do{
        cout<<" Please, enter the width: ";
            cin>>width;
        cout<<" Please, enter the length: ";
            cin>>length;
    }while(isNegative(width, length));

    // Create a 2D dynamically allocated array.
    int **p_p_multiplicationTab = new int*[width];
    for(int i = 0; i < width; i++)
        p_p_multiplicationTab[i] = new int[length];

    // Here you operate on data.
    fillArray(p_p_multiplicationTab, width, length);
    printArray(p_p_multiplicationTab, width, length);

    // Delete a 2D dynamically allocated array.
    for(int i = 0; i < width; i++)
       delete []p_p_multiplicationTab[i];
    delete []p_p_multiplicationTab;
    p_p_multiplicationTab = NULL;
}

void fillArray(int **p_p_array, int width, int length){
    for(int i = 0; i < width; i++)
        for(int j = 0; j < length; j++)
            p_p_array[i][j] = (i+1)*(j+1);
}

void printArray(int **p_p_array, int width, int length){
    for(int i = 0; i < width; i++)
        for(int j = 0; j < length; j++)
            cout<<" "<<i+1<<" * "<<j+1<<" = "<<p_p_array[i][j]<<endl;
}

bool isNegative(int width, int length){
    if(width <= 0 || length <= 0){
        cout<<"\n Error: The entered values must be greater then zero \n";
        return true;
    }
    return false;
}


