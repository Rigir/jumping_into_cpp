/* 
    Practise Problem
        4. Write a program that lets users keep track of the last time they talked to each of their friends.
           Users should be able to add new friends (as many as they want!) and store the number of days
           ago that they last talked to each friend. Let users update this value (but don't let them put in
           bogus numbers like negative values). Make it possible to display the list sorted by the names of
           the friends of by how recently it was since they talked to each friend.
*/

#include <iostream>
#include <string>

using namespace std;

//Structures
struct person{
    string name = " ";
    int lastCall = 0;
};

//Function headers
bool isNegative(int mun);
void sortBy(person *p_array, int elementsSet, char option);
void displayMenu(person p_addressBook[], int size);
void addNewPerson(person *p_array, int elementsSet);
void updateLastCall(person p_addressBook[], int size);
person *growArray(person *p_array, int *size);


//The program starts here
int main(){
    int menu_choice, elementsSet = 0, size = 4;
    person *p_addressBook = new person[size];
    
    while(true){
        cout<<"s: "<<size<<" eS: "<<elementsSet+1<<endl;
        displayMenu(p_addressBook, size);

        cout<<"\n Please, enter your choise: ";
            cin>>menu_choice;

        switch(menu_choice){
            case 1: 
                if(size == elementsSet+1)
                    p_addressBook = growArray(p_addressBook, &size);  
                addNewPerson(p_addressBook, elementsSet); 
                elementsSet++;           
                break;
            case 2:
                updateLastCall(p_addressBook, size);
                break;
            case 3: 
                cout<<" Do you want sort by ? \n 1. Name | 2. Number of days | ";
                    cin>>menu_choice;
                    
                    switch (menu_choice){
                        case 1:
                            sortBy(p_addressBook, elementsSet,'n');
                            break;
                        case 2: 
                            sortBy(p_addressBook, elementsSet,'d');
                            break;
                        default: cout<<"\n Error: Bad input! Try again!\n";
                    }
                break;
            case 4: 
                cout<<" It's now safe to turn off your computer! \n";
                return true;
            default:            
                cout<<"\n Error: Bad input! Try again!\n";
                break;
        }
    }
\
    delete[] p_addressBook;
    p_addressBook = NULL;
}

void sortBy(person *p_array, int elementsSet, char option){
    person temp;
    bool lastCall, name;
    for(int i = 0; i < elementsSet-1; i++)
        for(int j = 0; j < elementsSet-i-1; j++){
            lastCall = p_array[j].lastCall > p_array[j+1].lastCall;
            name =  p_array[j].name > p_array[j+1].name;
            if((lastCall && option == 'd' ) || (name && option == 'n' )){
                temp = p_array[j];
                p_array[j] = p_array[j+1];  
                p_array[j+1] = temp; 
            }
        }
}


void updateLastCall(person p_addressBook[], int size){
    char answer;
    int id, num;

    do{
        cout<<"\n Enter a id of person you looking for: ";
            cin>>id;

        cout<<" Are you looking for: \n "<<id<<"\t "<<p_addressBook[id-1].name<<" "<<p_addressBook[id-1].lastCall<<endl;
        cout<<" Did you mean this person? ( Y/N ) | ";
            cin>>answer;
    }while( answer == 'N' || answer == 'n');

    do{ 
        cout<<" Enter a new number of days since the last call: ";  
            cin>> num;
    }while(isNegative(num));
    p_addressBook[id-1].lastCall = num;
}

bool isNegative(int num){
    if(num < 0){
        cout<<"\n Error: The number of days since the last call cannot be negative.\n";
        return true;
    }return false;
}

void addNewPerson(person *p_array, int elementsSet){
    cout<<"\n Please, enter:\n First Name: ";
            cin>>p_array[elementsSet].name;
    do{
        cout<<" Number: ";  
            cin>>p_array[elementsSet].lastCall;
    }while(isNegative(p_array[elementsSet].lastCall));
}

void displayMenu(person p_addressBook[], int size){
    cout<<"\n\t\t\t  Address Book \n";
    for(int i=0; i<size; i++){
        if(p_addressBook[i].name != " ")
            cout<<"  "<<i+1<<"\t "<<p_addressBook[i].name<<" "<<p_addressBook[i].lastCall<<endl;
    }
    cout<<"\n| 1 - Add contact | 2 - Update last call | 3 - Sort options  | 4 - Exit |\n";
}

person *growArray(person *p_array, int *size){
	*size *= 2;
	person *p_new_array = new person[ *size ];
	for ( int i = 0; i <*size/2 ; i++ )
		p_new_array[ i ] = p_array[ i ];
	delete[] p_array;
	return p_new_array;
}
